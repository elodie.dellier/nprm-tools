# merge de tous les fichiers
python main_merge.py -i D:\fichiers_201219 -o D:\

# profiling des fichiers
pandas_profiling NPRM_SEMIS_FUSION_09012020.csv ./data/results/Report_SEMIS_090120.html
pip install -r wheelhouse/requirements.txt --no-index --find-links wheelhouse


py -3.7 main_find_subgroup.py -i "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 16\Fichiers fusionnés\SCENARIO_16_CANEVAS_SEMIS_FUSION_FORMATE_.csv"

py -3.7 main_find_subgroup.py -i "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12\Fichiers fusionnés\SCENARIO_12_CANEVAS_SEMIS_FUSION_FORMATE_.csv"

py -3.7 main_find_subgroup.py -i "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12.1\Fichiers fusionnés v2\SCENARIO_12.1_CANEVAS_SEMIS_FUSION_FORMATE_SURECH.csv"

py -3.7 main_find_subgroup.py -i "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12.2\Fichiers fusionnés v2\SCENARIO_12.2_CANEVAS_SEMIS_FUSION_FORMATE__newProj.csv"

py -3.7 main_find_subgroup.py -i "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 6.2\Fichiers fusionnés v2\SCENARIO_6_CANEVAS_SEMIS_FUSION_FORMATE__PROJ_POND_112600.csv"

py -3.7 main_find_subgroup.py -i "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 6.2\Fichiers fusionnés v2\SCENARIO_6_CANEVAS_SEMIS_FUSION_FORMATE__PROJ_POND_112600.csv" -c "config\param_scenario_6.2_sub.json"


py -3.7 main_postprocess.py --dataset "C:\Users\NPRM-TMP\Desktop\NPRM\nprm-python\Résultats\Scénario 12\REGLES_SCENARIO_12_0129-1513_SimpleDFSW_10_196733_INPUT_MODALITE.csv" -i "C:\Users\NPRM-TMP\Desktop\NPRM\nprm-python\Résultats\Scénario 12\SimpleDFSWithPruning_multiProcess_1000Bucket_2_n15_n5_196733.csv"

py -3.7 main_postprocess.py --dataset "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 16\Fichiers fusionnés\SCENARIO_16_CANEVAS_SEMIS_FUSION_FORMATE_.csv" -i "C:\Users\NPRM-TMP\Desktop\NPRM\nprm-python\Résultats\Scénario 16\SDMap_500Bucket_2_n15_n5_196738.csv"


py -3.7 main_merge.py -s "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 16\Semis" -c "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Canevas_sans_DS_OK" --scenario SCENA_16_TEST

py -3.7 main_merge.py -s "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12.1\Semis" -c "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12.1\Canevas" --scenario SCENARIO_12.1

py -3.7 main_merge.py -s "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12.2\Semis" -c "C:\Users\NPRM-TMP\Desktop\NPRM\Fichiers\Scenario 12.2\Canevas" --scenario SCENARIO_12.2


