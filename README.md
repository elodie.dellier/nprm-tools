# Outils pour le traitement de fichiers xlsx

## Merge de fichiers Input et Output

### Pour une machine sans environnement python 

#### Build l'exe 

```
build_exe.bat
```

Cela créé un exe dans le dossier "dist" appelé "merge_semis_caneva.exe"


#### Appel de merge_semis_caneva

Se positionner dans le dossier qui contient "merge_semis_caneva.exe"  ou alors ajouter le chemin avant le nom.

```
merge_semis_caneva.exe -i D:\fichiers_201219 -o D:\
```

- -i : le chemin du dossier parent qui contient les fichiers, recherche récursive
- -o: le chemin où sera enregistré les fichiers mergés (pour l'instant il faut le créer manuellement si le dossier n'existe pas) par défaut: "./"

Un oubli dans la commande :

```
merge_semis_caneva.exe -h
```

### Pour une machine avec un environnement python 

### Installation des dépendances 

```
python -m pip install -r requirements.txt
```

#### Appel de main_merge (== merge_semis_caneva en version python)


```
python main_merge.py -i D:\fichiers_201219 -o D:\
```

- -i : le chemin du dossier parent qui contient les fichiers, recherche récursive
- -o: le chemin où sera enregistré les fichiers mergés (pour l'instant il faut le créer manuellement si le dossier n'existe pas) par défaut: "./"

Un oubli dans la commande :

```
python main_merge.py  -h
```

### Téléchargement des packages

mkdir wheelhouse && pip download -r requirements.txt -d wheelhouse