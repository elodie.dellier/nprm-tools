import argparse

import os
import numpy as np 

import time
import datetime as dt
import re 
import json 
import pandas as pd


import pysubgroup_parallel as ps
from matplotlib import pyplot as plt
from pysubgroup_parallel import visualization as ps_viz

def cond_minArm(x):
    cond_minArm = ((x["SITMIL.ARMEE"] != "305") &  # GND 
                   (x["SITMIL.CORPS"] != "9164") & 
                   (x["SITMIL.CORPS"] != "9969") &
                   (x["SITMIL.ARMEE"] != "310"))  # affMaritimes
    return cond_minArm

def chrono(function, *args, **kwargs):
    print("Starting algorithm chrono at %s" % (dt.datetime.now()))
    a, result, b = time.time(), function(*args, **kwargs), time.time()
    return b-a, result


def search_other_config(result, cle='coverage_sg', seuil=0.50):
    ind, sg_max = max(enumerate(result), key=lambda item: item[1][1].statistics[cle])
    if sg_max[1].statistics[cle] < seuil:


def find_subgroup(find_subgroup_function, df, algo, quality_factor, a, depth, 
                              target, ignore_list, size, size_min_pop, **kwargs):
    print("Parameters: \n")
    print("\tAlgo used        : %s " % algo.__name__)
    print("\tTarget           : %s" % target)
    print("\tQuality factor   : %s(%s)" % (quality_factor.__class__.__name__, a))
    print("\tDepth            : %d" % depth)
    print("\tIgnored features : %s" % ignore_list)

    execution_time, results = chrono(find_subgroup_function, df, algo, quality_factor, depth, target, 
                                    ignore_list, size, size_min_pop, **kwargs)
     
    print("Code executed in %.2f seconds.\n" % execution_time)
    print(f"Results: {len(results)} \n")

    return results, execution_time


def display_results(path_output, df, results, type_target="nominal", with_display=True, nb_result_max_plot=100):
   
    os.makedirs(path_output, exist_ok=True)
    if type_target == "nominal":
        statisticsToShow = ps.all_statistics
    else:
        statisticsToShow = ps.all_statistics_numeric

    results_df = ps.utils.results_as_df(df, results, statistics_to_show=statisticsToShow, autoround=True,
                                        include_target=False)
    results_df_plot = results_df.head(nb_result_max_plot)
    results_plot = results[:nb_result_max_plot]

    for ind, (q, sg) in enumerate(results):
        print(ind, ":", q, sg.subgroup_description, sg.get_base_statistics(df))

    results_df.to_csv(path_output+"_df_fr.csv", sep=";", decimal=",")

    ####### # Visualisation (juste pour le cas des nominal)
    if with_display:
        path_and_prefixe = os.path.join(path_output, os.path.basename(path_output))
        fig_sgbars = ps_viz.plot_sgbars(results_df_plot, False)
        
        fig_roc = ps_viz.plot_roc(results_df_plot, df)
        fig_npspace = ps_viz.plot_npspace(results_df_plot, df)
        
        print("Debut intersection similarity")
        fig_similarity, indices_sg_diff, score_simi = ps_viz.similarity_dendrogram(results_plot, df)

        print("Debut union similarity")
        fig_union = ps_viz.union_dendrogram(results_plot, df, indices_sg_diff)
        # plt.show()

        fig_npspace.set_size_inches(25, 15)
        fig_union.set_size_inches(25, 15)
        fig_similarity.set_size_inches(25, 15)

        #######################################""
        fig_roc.savefig(path_and_prefixe + '_ROC.png')
        fig_npspace.savefig(path_and_prefixe + '_npspace.png')
        fig_sgbars.savefig(path_and_prefixe + '_sgbars.png')
        fig_union.savefig(path_and_prefixe + '_union.png')
        fig_similarity.savefig(path_and_prefixe + '_similarity.png')

        score_simi.index = score_simi.columns
        score_simi.to_csv(os.path.join(path_output, "_score_simi_fr.csv"), sep=";", decimal=",")
        

def b4_nprm_acquise(x):
#     Pour la colonne SITMIL.BREVET_MILITAIRE   elle doit être combiner avec la colonne » QUALIFICATION_MIL.PHT, 
#     alors si il y a une valeur dans une des deux colonnes c’est que la balise B4 est acquise pour les FAFR suivante : Marine, ADT, SEA, AIR.

# Pour le SSA il ne faut prendre que la colonne QUALIFICATION_MIL.PHT qui est la B4
# Pour la gendarmerie ne prendre que la PHT également . car attention la colonne SITMIL.BREVET_MILITAIRE   pour la gendarmerie sert pour la B3.

    if x["SITMIL.ARMEE"] in ["302", "303", "304", "309"]:  # Marine, ADT, SEA, AIR
        return ((x["SITMIL.BREVET_MILITAIRE"] > 0) | (x["QUALIFICATION_MIL.PHT"] > 0))
    elif x["SITMIL.ARMEE"] in ["305", "307"]:  # Gendarmerie, SSA
        return (x["QUALIFICATION_MIL.PHT"] > 0)
    else:
        return False


def difference_balise(x_v1, x_v2):
    if x_v1 is True and x_v2 is False:
        return "PERTE_BALISE"
    elif x_v1 is True and x_v2 is True:
        return "BALISE_MAINTENUE"
    elif x_v1 is False and x_v2 is True:
        return "GAIN_BALISE"
    else:
        return "SANS_BALISE"


def get_statut_balise(df):
    df["B1_PPCR"] = df["PSOFF"] > 0
    df["B1_NPRM"] = df["V2_PQMSB1"] > 0 # df.apply(lambda x: x["V2_PQMSB1"] > 0, axis=1)

    df["B2_PPCR"] = False
    df["B2_NPRM"] = df["V2_PQMSB2_AVC_PLAF"] > 0 

    df["B3_PPCR"] = df[["PQUAL16", "CLAIR_ARMEE", "SITMIL.BREVET_MILITAIRE"]].apply(lambda x: (x["PQUAL16"] > 0) |
                                       ((x["CLAIR_ARMEE"] == "Gendarmerie") & (x["SITMIL.BREVET_MILITAIRE"] > 0)), axis=1)
    df["B3_NPRM"] = df["V2_PQMSB3_AVC_PLAF"] > 0

    df["B4_PPCR"] = df.apply(lambda x: b4_nprm_acquise(x), axis=1)
    df["B4_NPRM"] = df["V2_PQMSB4"] > 0

    df["QUAL28_PPCR"] = df["PQUAL28"] > 0
    df["QUAL28_NPRM"] = df["V2_PQUAL28"] > 0

    df["AUTRE_QUAL_54_PPCR"] = df["AUTRES_QAL_54"] > 0
    df["AUTRE_QUAL_54_NPRM"] = df["V2_AUTRES_QAL_54"] > 0

    df["B1_STATUT"] = df[["B1_PPCR", "B1_NPRM"]].apply(lambda x: difference_balise(x["B1_PPCR"], x["B1_NPRM"]), axis=1)
    df["B2_STATUT"] = df[["B2_PPCR", "B2_NPRM"]].apply(lambda x: difference_balise(x["B2_PPCR"], x["B2_NPRM"]), axis=1)
    df["B3_STATUT"] = df[["B3_PPCR", "B3_NPRM"]].apply(lambda x: difference_balise(x["B3_PPCR"], x["B3_NPRM"]), axis=1)
    df["B4_STATUT"] = df[["B4_PPCR", "B4_NPRM"]].apply(lambda x: difference_balise(x["B4_PPCR"], x["B4_NPRM"]), axis=1)
    df["QUAL28_STATUT"] = df[["QUAL28_PPCR", "QUAL28_NPRM"]].apply(lambda x: difference_balise(x["QUAL28_PPCR"], x["QUAL28_NPRM"]), axis=1)
    df["AUTRE_QUAL_54_STATUT"] = df[["AUTRE_QUAL_54_PPCR", "AUTRE_QUAL_54_NPRM"]].apply(lambda x: difference_balise(x["AUTRE_QUAL_54_PPCR"], x["AUTRE_QUAL_54_NPRM"]), axis=1)

    return df


def preprocess_data(df):
    # formatage des colonnes
    for col in ["STATS.GENRE", "SITMIL.ARMEE", "SITMIL.CORPS", "SITMIL.GRADE",  "AFFECTATION.ZONE", "AFFECTATION.MICMZONEGEO"]: 
        df[col] = df[col].apply(str)  
 
    df = get_statut_balise(df)
    df["INFO_COUPLE_MILI"] = df["INFO_COUPLE_MILI"].str.replace(r'\W+', ' ')
    df["INFO_COUPLE_MILI"] = df['INFO_COUPLE_MILI'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

    df[["Libellé", "SITMIL.ARMEE", "INFO_COUPLE_MILI"]].to_csv("statut_balise.csv", sep=";", decimal=",")

    return df


def find_subgroup_numeric(data, algo, quality_factor, depth, target_label, ignore_list, number_rules_max=10, 
                          result_min_size=10, **kwargs):
    list_work = list(set(ignore_list.copy()))
    if target_label not in list_work:
        list_work.append(target_label)
    print(f"Data dimensions : {len(data)}, {len(data.columns)-len(list_work)}, {data.shape}")
    
    target = ps.NumericTarget(target_label)
    search_space = ps.create_selectors(data, ignore=list_work, nbins=5)
    task = ps.SubgroupDiscoveryTask(data, target, search_space, result_set_size=number_rules_max,
                                    depth=depth, qf=quality_factor, result_min_size=result_min_size)
    
    if algo == ps.algorithms.BeamSearch:
        # pour que l'arbre soit de la même taille que les données
        result = algo(number_rules_max).execute(task, **kwargs)
    else:
        result = algo().execute(task, **kwargs)
    return result


def find_subgroup_nominal(data, algo, quality_factor, depth, target_label, ignore_list, number_rules_max=10, 
                          result_min_size=10, **kwargs):
    list_work = list(set(ignore_list.copy())) 
    if target_label not in list_work: 
        list_work.append(target_label)
    print(f"Data dimensions : {len(data)}, {len(data.columns)-len(list_work)}, {data.shape}")
    
    target_value = True
    target = ps.NominalTarget(target_label, target_value)
    search_space = ps.create_selectors(data, ignore=list_work, nbins=5)
  
    task = ps.SubgroupDiscoveryTask(data, target, search_space, result_set_size=number_rules_max,
                                    depth=depth, qf=quality_factor, result_min_size=result_min_size)
    
    if algo == ps.algorithms.BeamSearch:
        # pour que l'arbre est la même taille que les données
        result = algo(number_rules_max).execute(task, **kwargs)
    elif algo == ps.sdmap_algorithm.SDMap: 
        total_population_size = len(data)
        defined_positives = len(data[data[target_label] == target_value])
        result = algo(task, target, total_population_size, defined_positives).execute()
    else:
        result = algo().execute(task, **kwargs)
        
    result.sort(key=lambda x: [x[0], x[1].subgroup_description], reverse=True)
    return result


def build_subgroup():
    # à changer 
    list_function = {}
    list_function["Bucket_0_all"] = lambda x: True
    list_function["Bucket_0_all_numeric"] = lambda x: x
    list_function["Bucket_1_n100_n15"] = lambda x: x < -15 or x is None
    
    list_function["Bucket_1_n100_n15"] = lambda x:  x < -15
    list_function["Bucket_2_n15_n5"] = lambda x: x >= -15 and x < -5
    list_function["Bucket_3_n5_n0"] = lambda x: x >= -5 and x < 0
    list_function["Bucket_4_p0_p5"] = lambda x: x >= 0 and x < 5
    list_function["Bucket_5_p5_p10"] = lambda x: x >= 5 and x < 10
    list_function["Bucket_6_p10_p15"] = lambda x: x >= 10 and x < 15
    list_function["Bucket_7_p15_p100"] = lambda x: x >= 15
    
    list_function["Bucket_3_n5_p5"] = lambda x: x >= -5 and x < 5
    list_function["Bucket_4_p5_p15"] = lambda x: x >= 5 and x < 15
    list_function["Bucket_4a_p5_p10"] = lambda x: x >= 5 and x < 10
    list_function["Bucket_4b_p10_p15"] = lambda x: x >= 10 and x < 15
    list_function["Bucket_5_p15_p100"] = lambda x: x >= 15
    list_function["Bucket_perdant"] = lambda x: x < 0
    list_function["Bucket_gagnant"] = lambda x: x >= 0

    return list_function


def process_subgroup(nom_scenario, data_sg, data_all, target, find_subgroup_method=find_subgroup_nominal, 
                     number_rules_max=100, 
                     name_algo=None, size_min_pop=10, list_bucket=None,
                     size_dataset=None, dir_output="./Find_SG_Results", 
                     depth=3, ignore_list=[], **kwargs):


    # Main parameters  
    a = 0.5
    quality_factor = ps.StandardQF(a) if str(find_subgroup_method.__name__) == "find_subgroup_nominal" \
                    else ps.StandardQFNumeric(a)

    ignore_list = ["Libellé", target, "MODALITE"] + ignore_list

    data_work = data_sg if size_dataset is None else data_sg.head(size_dataset)

    parameters = { 
        'number_rules_max': number_rules_max,
        'name_algo': name_algo,
        'size_min_pop': size_min_pop,
        'list_bucket': list_bucket,
        'quality_factor': "%s(%s)" % (quality_factor.__class__.__name__, a),
        'depth': depth,
        'ignored_features': ignore_list,
        'shape_data_sg': data_sg.shape, 
        'shape_data': data_all.shape, 
        'shape_data_work': data_work.shape,
        'size_dataset': size_dataset,
        'cond_filtrage_sg_description': kwargs.get("cond_filtrage_sg_description", ""),
        'cond_filtrage': kwargs['cond_filtrage']
    }

    name_algo = eval(name_algo)  # convert le string en fonction
    print(parameters)
    
    list_usecase = build_subgroup()
    list_usecase_bucket_filter = {x: val for x, val in list_usecase.items() if x in parameters['list_bucket']} \
                                  if parameters['list_bucket'] is not None \
                                  else list_usecase
    
    optional = {"nb_processes": 4} if isinstance(name_algo, ps.SimpleDFSWithPruning_multiProcess) else {}
    date_str = dt.datetime.now().strftime("%m%d-%H%M")

    os.makedirs(dir_output, exist_ok=True)
    deb_nom_file = "REGLES_" + nom_scenario + "_" + date_str + "_" + name_algo.__name__[:10] + "_" + \
        find_subgroup_method.__name__[-7:] + "_" + str(parameters['number_rules_max']) + \
        "_" + str(data_work.shape[0])
    deb_nom_file = os.path.join(dir_output, deb_nom_file)

    data_out = data_work.copy()
    data_out["MODALITE"] = "AUCUNE MODALITE"

    param_config_to_run = []
    # Exectution de l'algo par modalité
    for key_target, function in list_usecase_bucket_filter.items():
        df_work_subgroup_tmp = data_work.copy()
        df_work_subgroup_tmp[key_target] = df_work_subgroup_tmp[target].apply(function)
        data_all[key_target] = data_all[target].apply(function)
        ignore_list.append(key_target)
        
        print("*"*10, key_target, "*"*10, df_work_subgroup_tmp.shape)
        
        key_target_true = key_target if str(find_subgroup_method.__name__) == "find_subgroup_nominal" else target
        if df_work_subgroup_tmp[df_work_subgroup_tmp[key_target]].shape[0] > 0:
            name_output = deb_nom_file + "_" + key_target
           
            results, execution_time = find_subgroup(find_subgroup_method, df_work_subgroup_tmp,
                                                    name_algo, quality_factor, a, depth, key_target_true, 
                                                    ignore_list, parameters['number_rules_max'], size_min_pop,
                                                    **optional)
            display_results(name_output, data_all, results, type_target="nominal")

            data_out[key_target + "_val"] = df_work_subgroup_tmp.apply(lambda x: x[target] if x[key_target] == True else 0, axis=1)
            data_out.loc[df_work_subgroup_tmp[key_target]==True, "MODALITE"] = key_target
                        
            parameters[key_target] = {'time': round(execution_time, 2),
                                      'shape_data': df_work_subgroup_tmp.shape}

            # recherche du complement à runner
            # config_to_run = search_other_config(results)
            # if config_to_run is not None:
            #     param_config_to_run.append(config_to_run[1])
        else:
            print("*"*10, "ERREUR il n'y a aucune donnée")

        with open(deb_nom_file + "_param.json", 'w') as fic:  # pour éviter d'attendre toutes les modalites
            json.dump(parameters, fic, indent=4)

    with open(deb_nom_file + "_param.json", 'w') as fic:
        json.dump(parameters, fic, indent=4)

    data_out.to_csv(deb_nom_file + "_INPUT_MODALITE.csv", index=False, float_format='%.3f')
    return param_config_to_run


def get_parameters_tab_default():
    # exemple de parametres 
    parameters_def = [ 
        {'name': "_minArm_",
            'target_run': "GAIN-PERTE_%_TOTAL_ROUNDED",  # facultatif
            'list_key_pysubgroup': ['STIMIL.GRADE'],  # facultatif,  liste des colonnes à prendre pour les règles
            'depth': 3,
            'number_rules_max': 1000,  
            'name_algo': "ps.SimpleDFSWithPruning_multiProcess",
            'size_min_pop': 1,
            'list_bucket': ["Bucket_2_n15_n5"],
            'size_dataset': None, 
            'cond_filtrage': ['cond_minArm(x)', '&' 
                            '((x["_CELIBATAIRE_FAMILLE"]!="FAMILLE") | (x["AFFECTATION.MICM"]!=False))', '&',
                            '(x["INFO_COUPLE_MILI"]!="Couple 2 d\'un même SIRH")']},  
        
        {'name': "_gnd_",
         'number_rules_max': 1000,  
         'name_algo': "ps.SimpleDFSWithPruning_multiProcess",
         'size_min_pop': 1,
         'list_bucket': ["Bucket_2_n15_n5"],
         'size_dataset': None, 
         'cond_filtrage': ['cond_minArm(x)', '&' 
                            '((x["_CELIBATAIRE_FAMILLE"]!="FAMILLE") | (x["AFFECTATION.MICM"]!=False))', '&',
                            '((x["SITMIL.GRADE"]!="GS") | (x["_CELIBATAIRE_FAMILLE"]!="CELIBATAIRE"))']
        }
    ]
    return parameters_def


def main(path_data, path_config):
    if path_config is None: 
        path_config = "config/_param_defaut.json"
        print("Utilisation de la config par defaut ", path_config)
        with open(path_config, 'w') as fic:
            json.dump(get_parameters_tab_default(), fic, indent=4)

    df = pd.read_csv(path_data)
   
    print(f"Lecture de {path_data} de forme {df.shape}")

    df = preprocess_data(df)
    target = "GAIN-PERTE_%_TOTAL_ROUNDED"
    list_key_pysubgroup = [
        "Libellé", "STATS.GENRE", "STATS.COUPLE_MILITAIRE", "_CELIBATAIRE_FAMILLE", "_NB_ENFANT_TOTAL_CF",
        "SITMIL.ARMEE", "SITMIL.CORPS",  "SITMIL.ANC_SERVICE", "SITMIL.BREVET_MILITAIRE", "SITMIL.B2_GENDARME",
        "AFFECTATION.AFFECTATION", "_AFFECTATION.ZONE", "SITMIL.GRADE",
        "AFFECTATION.HOPITAL",
        "AFFECTATION.LOGEMENTGRATUIT", "AFFECTATION.MICM",  "MOBILITE.LOGEGRATUIT", "MOBILITE.NBMUT",
        "_ACTIVITE_SPECIFIQUE_V1", "_DIVERS_INFIRMIER", "RESP_ET_PERF.NBI", "_ACTIVITE_SPECIFIQUE_V2", "_ENGAGEMENT_OPS_V1",
        "_QUALIFICATION_OUI_NON",                
                        target]
    
    list_key_pysubgroup = [
        "Libellé", "STATS.GENRE", "SITMIL.ARMEE", "SITMIL.CORPS", "SITMIL.GRADE", "SITMIL.ANC_SERVICE", 
        "SITFAM.SITFAM", "AFFECTATION.AFFECTATION", "TOUCHE_LA_MICM", "V2_GARNISON.NBRE_PERSFOYER", "V2_GARNISON.ZONE_AFFECTATION", 
        "MOBILITE.NBMUT", "_QUALIFICATION_OUI_NON", "_ACTIVITE_SPECIFIQUE_V2", "INFO_CATEGORIE", 
        "INFO_COUPLE_MILI", "INFO_LOG_GRATUIT", "LOG_DEF", "TOUCHE_GARNISON_ISOLEE", 
        "B1_STATUT", "B2_STATUT", "B3_STATUT", "B4_STATUT", "QUAL28_STATUT", "AUTRE_QUAL_54_STATUT",
        target]

    list_to_add = [
                    "GAIN-PERTE_%_INDICIARE_ROUNDED",
                    "GAIN-PERTE_INDICIARE",
                    "GAIN-PERTE_%_ETAT_MILITAIRE-GARNISON_ROUNDED",
                    "GAIN-PERTE_ETAT_MILITAIRE-GARNISON",
                    "GAIN-PERTE_%_PARCOURS-PRO_ROUNDED",
                    "GAIN-PERTE_PARCOURS-PRO",
                    "GAIN-PERTE_TOTAL"]

    df_work_subgroup = df[list_key_pysubgroup + list_to_add]
    print("-"*10, "Description du dataframe final", "-"*10)
    print(df_work_subgroup.shape)
   
    nom_scenario = re.findall(r'(SCENARIO_\d*\.?\d*)', os.path.basename(path_data))[0]
    
    if path_config is None : 
        path_config = "config/_param_defaut.json"
        print("Utilisation de la config par defaut ", path_config)
        with open(path_config, 'w') as fic:
            json.dump(get_parameters_tab_default(), fic, indent=4)

    with open(path_config, 'r') as fic:
        params_tab = json.load(fic)
    
    for ind, params in enumerate(params_tab):
        if params.get('execute', "") == "False":
            continue
        target = params.get('target_run', target)
        if len(params.get('list_key_pysubgroup', [])) > 0:
            # changement des colonnes à consider pour la recherche de subgroup
            df_work_subgroup = df[list(set(params['list_key_pysubgroup'] + list_to_add + ["Libellé", target]))]

        print("Run du scénario nommé :", params.get('name', ""), "avec la cible ", target)
        cond_filtrage = "".join([cond for cond in params.get('cond_filtrage', None)])
        cond_apply = df_work_subgroup.apply((lambda x: eval(cond_filtrage)), axis=1)
        
        print("after apply")
        df_work = df_work_subgroup[(cond_apply)] \
                    if cond_filtrage is not None \
                    else df_work_subgroup
        if "cond_filtrage_sg" in params:
            # pour les sousgroups trouvés dans les itérations précédantes
            cover_sg = params["cond_filtrage_sg"].subgroup_description.covers(df_work) 
            print(f"Size suppression data {df_work.shape[0] - np.sum(cover_sg)}")
            df_work_subgroup_sg = df_work[params["cond_filtrage_sg"].subgroup_description.covers(df_work) == False]
        else:
            df_work_subgroup_sg = df_work
            
        print("debug", params)
        sg_desc = f"_c{ind}_" if "cond_filtrage_sg_description" in  params else ""
        config_to_run = process_subgroup(nom_scenario + params.get('name', "") + sg_desc, df_work_subgroup_sg, df_work, target,
                         find_subgroup_nominal, ignore_list=list_to_add, **params)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage="merge -i path_semis_fusion")
    parser.add_argument("--input", "-i", help="")
    parser.add_argument("--config", "-c", help="path du fichier config JSON pour parametrer les runs", default=None)
    args = parser.parse_args()
    print(args.input, args.config)
    main(args.input, args.config)
