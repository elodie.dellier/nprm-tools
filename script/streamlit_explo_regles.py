import pandas as pd
import streamlit as st
import os
import re
import numpy as np
import argparse
import json 
import plotly.graph_objects as go


def file_selector_data():
    filename = st.sidebar.file_uploader(label="Selectionne le fichier d'entrée des règles (_MODALITE.csv)", type="csv")
    # st.write(f"You selected {filename}")
    if filename is not None:
        data = pd.read_csv(filename)
        st.write("Aperçu des 10 premières lignes de _MODALITE.csv")
        st.write(data.head())
        return data
    return None


def file_selector_regles():
    filename = st.sidebar.file_uploader(label="Selectionne un jeu de règles (_df.csv)", type="csv")
    # st.write('You selected `%s`' % filename)
    if filename is not None:
        data = pd.read_csv(filename, sep=";", decimal=",")
        st.write("Détails des règles")
        st.write(data)
        return data
    return None
 

# @st.cache
def read_csv(file_path, size_show=5, sep=";", decimal=","):
    data = pd.read_csv(file_path, sep=sep, decimal=decimal)
    st.write(os.path.basename(file_path))
    st.write(data.head(size_show))
    # st.write(data.dtypes)
    return data


def split_convert_rules(row):
    nb_rules = row.split("AND")
    rules_splitted = list()
    for rule in nb_rules:
        condition = rule.split("=")
        num_cond = re.match(r"(.*): \[(\d*.?\d*):(\d*.?\d*)\[",rule)
        cond_inf = rule.split("<")
        cond_sup = rule.split(">=")
        
        if len(cond_sup) > 1:
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": cond_sup[0].strip(),
                "type": "BORNE",
                "value_min": float(cond_sup[1]),
                "value_max": float('Inf'),
            })
        elif len(condition) == 2:
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": condition[0].strip(),
                "type": "COND",
                "value": condition[1].strip()
            })
        elif num_cond is not None:
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": num_cond.group(1).strip(),
                "type": "BORNE",
                "value_min": float(num_cond.group(2)),
                "value_max": float(num_cond.group(3)),
            })       
        elif len(cond_inf) > 1:
            rules_splitted.append({
                "name": rule.strip(),
                "colonne": cond_inf[0].strip(),
                "type": "BORNE",
                "value_min": -float('Inf'),
                "value_max": float(cond_inf[1]),
            })
        else:
            raise Exception(f"[{rule}] n'est pas traitée par la parsage")
    return rules_splitted


def get_funct_from_str(rules):
    if rules["type"] == "BORNE":
        funct = lambda x: rules["value_min"] <= float(x[rules["colonne"]]) and float(x[rules["colonne"]]) < rules["value_max"]
    else:  # cond
        funct = lambda x: str(x[rules["colonne"]]) == str(rules["value"])
    return funct


def compute_selector(df_moda, rules_splitted):
    df_moda_process = df_moda.copy()
    sg = df_moda["MODALITE"] != "nothing"  # fausse condition pour obtenir une colonne de True
    for rule in rules_splitted:
        sg = sg & df_moda_process.apply(get_funct_from_str(rule), axis=1)
    return sg


# @st.cache
def update_sg_computed(sg_already_calc, ind, sg):
    if ind not in sg_already_calc:
        sg_already_calc[ind] = sg
    return sg_already_calc


def read_all_df(path_data, path_subgroup_file):
    df_modalite, regles_df = None, None
    regles_df = {}

    if path_data is None or path_subgroup_file is None:
        df_modalite = file_selector_data()
        df_regles = file_selector_regles()
        if df_regles is not None:
            regles_df["FS"] = df_regles
    else:
        df_modalite = read_csv(path_data, sep=",", decimal=";")
        
        if isinstance(path_subgroup_file, dict):
            path_regles = [(name, info["chemin_fichier"], info.get("indices", None)) for name, info in path_subgroup_file.items()]
        else:
            # en argument
            path_regles = [(os.path.basename(path_subgroup_file), path_subgroup_file, None)] 

        for name, path, indices in path_regles:
            df_cur = read_csv(path, size_show=1000, sep=";", decimal=".")
            regles_df[name] = (df_cur, indices)
    return df_modalite, regles_df


def make_dict_by_moda(stats):
    stats_by_modalite = {}
    for name_regle, info_regle in stats.items():
        for name_moda, modalite in info_regle.items():
            if isinstance(modalite, dict):
                if name_moda not in stats_by_modalite:
                    stats_by_modalite[name_moda] = {}
                    stats_by_modalite[name_moda][name_regle] = {}
                stats_by_modalite[name_moda][name_regle] = modalite
    return stats_by_modalite


def draw_count_intersecton(all_interection, count, seuil_min_pourcent=5):
    ############### Intersection ############################
    x = list(count.keys())
    # seuil_min_pourcent = 5
    fig = go.Figure(layout_title_text=f"Intersection des règles avec au minimun {seuil_min_pourcent}% pour un bucket")
    
    for ind, name_regle in enumerate(sorted(all_interection)):
        y_sg = []
        for moda, count_items in count.items():
            # print(count_items)
            val = count_items[name_regle] if name_regle in count_items else 0
            y_sg.append(val)
        
        if (max(y_sg) > seuil_min_pourcent):
            # if ind == 0:
            #     fig = go.Figure(go.Bar(x=x, y=y_sg, name=name_regle))
            # else:
            fig.add_trace(go.Bar(x=x, y=y_sg, name=name_regle.strip("_")))
    fig.update_layout(barmode='stack', legend_orientation="h") #, xaxis={'categoryorder':'category ascending'})    

    st.plotly_chart(fig, use_container_width=True)


def compute_intersection(save_sg_pos):
    count = dict()
    combi = dict()
    for name_moda, sg_by_moda in save_sg_pos.items():
        # combi = None
        for ind, (name_regle, sg_pos) in enumerate(sg_by_moda.items()):
            tmp_combi = np.where(sg_pos, name_regle, "")
            if ind == 0 :
                combi[name_moda] = tmp_combi
            else:
                combi[name_moda] = [f"{val_old}_{val}" for val_old, val in zip(combi[name_moda], tmp_combi)]
        
        from collections import Counter
        count_ = Counter(combi[name_moda]) 
        count[name_moda] = {i: val / len(combi[name_moda]) * 100.0 for i, val in count_.items()}
    all_interection = list(set([key for name_moda, val_mod in count.items() for key, _ in val_mod.items()]))
    return all_interection, count


def compute_stats_for_one_subgroup(sg, df_pos):
    nb_pos = int(np.sum(df_pos))
    stats_moda = {}
    stats_moda['nb_pos'] = nb_pos
    stats_moda['sg'] = int(np.sum(sg))
    stats_moda['sg_pos'] = int(np.sum(sg & df_pos))
    
    stats_moda['concentration'] = stats_moda['sg_pos'] / stats_moda['sg'] 
    stats_moda['couv'] = stats_moda['sg_pos'] / stats_moda['nb_pos']
    stats_moda['lift'] = stats_moda['concentration'] / (stats_moda['nb_pos'] / df_pos.shape[0])
    return stats_moda


def compute_all_subgroup(stats_moda, sg, df_pos, prev_sg):
    nb_pos = int(np.sum(df_pos))
    combi = {}
    
    combi['sg_union'] = int(np.sum(np.logical_or(sg, prev_sg)))
    combi['sg_intersection'] = int(np.sum(np.logical_and(sg, prev_sg)))
    combi['sg_similitude'] = combi['sg_intersection'] / combi['sg_union'] 
    combi['sg_recouvrement'] = combi['sg_intersection'] / stats_moda['sg']

    combi['sg_pos_union'] = int(np.sum(np.logical_or(sg & df_pos, prev_sg & df_pos)))
    combi['sg_pos_intersection'] = int(np.sum(np.logical_and(sg & df_pos, prev_sg & df_pos)))
    combi['sg_pos_similitude'] = combi['sg_pos_intersection'] / combi['sg_pos_union'] if combi['sg_pos_union'] > 0 else 0
    combi['sg_pos_recouvrement'] = combi['sg_pos_intersection'] / stats_moda['sg_pos'] if stats_moda['sg_pos'] > 0 else 0

    combi['union_concentration'] = combi['sg_pos_union'] / combi['sg_pos_union'] if combi['sg_pos_union'] > 0 else 0
    combi['union_couv'] = combi['sg_pos_union'] / nb_pos
    combi['lift'] = combi['union_concentration'] / (nb_pos / df_pos.shape[0])
    return combi


def main(path_data, path_subgroup_file, modalites=None):  
    df_modalite, df_regles = read_all_df(path_data, path_subgroup_file)
    indice_regles = {}
    if df_modalite is not None and df_regles is not None: 
        if modalites is None:
            modalites = st.sidebar.multiselect(
                'Quelle modalité?',
                sorted(df_modalite["MODALITE"].unique()), sorted(df_modalite["MODALITE"].unique()))
        for name_regles, (df, indices) in df_regles.items():
            if indices is None:             
                st.sidebar.markdown(name_regles)
                    
                indices = st.sidebar.multiselect(
                    'Quelles règles à analyser?', sorted(df['Unnamed: 0'].unique()))     
            indice_regles[name_regles] = indices

        prev_sg_moda = {}
        stats = {}
        temp_stats = {}
        save_sg_pos = {}
        # sg_already_calc = {}

        # st.write('Les règles sélectionnées sont les suivantes:', indice_regles)
        if st.sidebar.button('Go'):
            st.write("Indices : ", ", ".join([key + ": " + ",".join([str(ind) for ind in opt]) 
                     for key, opt in indice_regles.items()]))

            df_moda_positif = {}
            for modalite in modalites:
                temp_stats[modalite] = {}
                df_moda_positif[modalite] = df_modalite["MODALITE"] == modalite
                temp_stats[modalite]['nb_pos'] = int(np.sum(df_moda_positif[modalite]))
 
            for name_file_regle, liste_indice in indice_regles.items():
                df_regles_curr = df_regles[name_file_regle][0]
                regles_select = df_regles_curr.loc[df_regles_curr['Unnamed: 0'].isin(liste_indice)]

                for ind, regle_curr in regles_select.iterrows():
                    # st.write(regle_curr)
                    name_regles = regle_curr["subgroup"]
                    st.write(f"Indice en cours de calcul (attendre, la suite arrive) :{name_file_regle} - {regle_curr['Unnamed: 0']} - {name_regles}")
                   
                    rules_splitted = split_convert_rules(name_regles)

                    sg = compute_selector(df_modalite, rules_splitted)
                    
                    stats[f'{name_file_regle}_{ind}'] = {}

                    stats_regles_curr = {}
                    stats_regles_curr['indice_regle'] = f"R{regle_curr['Unnamed: 0']}"
                    stats_regles_curr['nom_fichier'] = name_file_regle
                    stats_regles_curr['nom_regle'] = name_regles

                    for modalite in sorted(modalites):
                        stats[f'{name_file_regle}_R{ind}'] = stats_regles_curr
                        df_pos = df_moda_positif[modalite]
                        stats_regles_curr[modalite] = compute_stats_for_one_subgroup(sg, df_pos)
                        
                        # st.write('taille', df_moda_process.shape[0])
                        gain_sg = {}
                        sg_pos = sg & df_pos
                        # st.write("df_pos", df_pos)
                        df_pos_gain = df_modalite.filter(regex=("GAIN-PERTE_*"), axis=1)
                        for (gain, val) in df_pos_gain.iteritems(): 
                            val[sg_pos].fillna(0, inplace=True)
                            gain_sg[gain] = np.nan_to_num(np.mean(val[sg_pos].astype('float')))

                        print(gain_sg)
                            # print(gain_sg)
                        stats_regles_curr[modalite]["gain"] = gain_sg
                        
                        if modalite in prev_sg_moda:
                            prev_sg = prev_sg_moda[modalite]
                            combi = compute_all_subgroup(stats_regles_curr[modalite], sg, df_pos, prev_sg)
                            temp_stats[modalite]["indices des regles de la combinaison"] += " avec " + str(ind)
                            combi['indices'] = temp_stats[modalite]["indices des regles de la combinaison"]
                            
                            stats[f'{name_file_regle}_R{ind}'][modalite]['combinaison'] = combi
                            prev_sg_moda[modalite] = np.logical_or(sg, prev_sg)
                            
                            sg_pos = np.logical_and(sg, df_pos)                           
                            save_sg_pos[modalite][f'{name_file_regle[-15:]}-R{ind}'] = sg_pos[df_pos]
                        else:
                            temp_stats[modalite]["indices des regles de la combinaison"] = str(ind)
                            prev_sg_moda[modalite] = sg
                            save_sg_pos[modalite] = dict()
                            sg_pos = np.logical_and(sg, df_pos)
                            only_pos = sg_pos[df_pos]
                            # st.write(len(sg_pos), len(only_pos))
                            save_sg_pos[modalite][f'{name_file_regle[-15:]}-R{ind}'] = only_pos

                        # sg_already_calc = update_sg_computed(sg_already_calc, ind, sg)

            stats_by_moda = make_dict_by_moda(stats)
            
            (all_interection, count) = compute_intersection(save_sg_pos)
            draw_count_intersecton(all_interection, count, seuil_min_pourcent=3)

            ###############    COUV ############################
            x = sorted(modalites)
            fig = go.Figure()
            for ind, (name_regle, stats_by_regle) in enumerate(stats.items()):
                y_sg = [stat['couv'] for name_moda, stat in stats_by_regle.items() if isinstance(stat, dict)]
                # print("bt regle", y_sg, stats_by_regle)
                # if ind == 0:
                #     fig = go.Figure(go.Bar(x=x, y=y_sg, name=stats_by_regle["nom_regle"]))
                # else:
                if len(y_sg) > 0 :
                    nom_legend = f'{stats_by_regle["nom_fichier"]}-{stats_by_regle["indice_regle"]}-{stats_by_regle["nom_regle"]}'

                    fig.add_trace(go.Bar(x=x, y=y_sg, name=nom_legend))
            fig.update_layout(barmode='stack', legend_orientation="h") #, xaxis={'categoryorder':'category ascending'})           
            st.plotly_chart(fig, use_container_width=True)
            
            # prev_sg = prev_sg | sg # prev_sg = sg & ou |
            st.write('stats', stats)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage="merge -i path_semis_fusion")
    parser.add_argument("--data", "-d", help="Chemin du fichier contenant les données d'entrée de la recherche de sous-groupe")
    parser.add_argument("--regle", "-r", help="Chemin ou fichier contenant les sous-groupes (règles)")
    parser.add_argument("--modalites", "-m", help="Modalités à visualiser", nargs='*')
    parser.add_argument("--config", "-c", help="Fichier de config pour tous les chemins et règles à sélectionner", 
                        default=None)
    args = parser.parse_args()

    params_tab = {}
    if args.config is not None:
        with open(args.config, 'r') as fic:
            params_tab = json.load(fic)
        
    path_data = params_tab.get("path_data", args.data)
    regles = params_tab.get("regles", args.regle)
    modalites = params_tab.get("modalites", args.modalites) # si on prend la config plus d'info!
    # liste_indices_regles = params_tab.get("liste_indices_regles", None)

    main(path_data, regles, modalites)


