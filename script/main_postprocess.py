import pandas as pd 
import re
import argparse
from collections import defaultdict 
import os

# duty à garder dans le csv d'entrée
target = "GAIN-PERTE_%_TOTAL_ROUNDED" 

def get_rule(rule):
    condition = rule.split("=")
    
    num_cond = re.match(r"(.*): \[(\d*.?\d*):(\d*.?\d*)\[",rule)
    cond_inf = rule.split("<")
    cond_sup = rule.split(">=")
    
    if len(cond_sup) > 1:
        return({
            "name" : rule.strip(),
            "colonne": cond_sup[0].strip(),
            "type": "BORNE",
            "value_min": float(cond_sup[1]),
            "value_max": float('Inf'),
        })
    elif len(condition) == 2 :
        return({
            "name" : rule.strip(),
            "colonne": condition[0].strip(),
            "type": "COND",
            "value": condition[1].strip()
        })
    elif num_cond is not None:
        return ({
            "name" : rule.strip(),
            "colonne": num_cond.group(1).strip(),
            "type": "BORNE",
            "value_min":float(num_cond.group(2)),
            "value_max": float(num_cond.group(3)),
        })       
    elif len(cond_inf) > 1:
        return ({
            "name" : rule.strip(),
            "colonne": cond_inf[0].strip(),
            "type": "BORNE",
            "value_min":-float('Inf'),
            "value_max": float(cond_inf[1]),
        })
    else :
        raise Exception(f"[{rule}] n'est pas traitée par la parsage")
    

def split_convert_rules(row):
    nb_rules = row.split("AND")
    rules_splitted = list()
    for rule in nb_rules:
        condition = rule.split("=")
        
        num_cond = re.match(r"(.*): \[(\d*.?\d*):(\d*.?\d*)\[", rule)
        cond_inf = rule.split("<")
        cond_sup = rule.split(">=")
        
        if len(cond_sup) > 1:
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": cond_sup[0].strip(),
                "type": "BORNE",
                "value_min": float(cond_sup[1]),
                "value_max": float('Inf'),
            })
        elif len(condition) == 2 :
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": condition[0].strip(),
                "type": "COND",
                "value": condition[1].strip()
            })
        elif num_cond is not None:
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": num_cond.group(1).strip(),
                "type": "BORNE",
                "value_min":float(num_cond.group(2)),
                "value_max": float(num_cond.group(3)),
            })       
        elif len(cond_inf) > 1:
            rules_splitted.append({
                "name" : rule.strip(),
                "colonne": cond_inf[0].strip(),
                "type": "BORNE",
                "value_min":-float('Inf'),
                "value_max": float(cond_inf[1]),
            })
        else:
            raise Exception(f"[{rule}] n'est pas traitée par la parsage")

    return rules_splitted


def calcul_stats_rules(df_data, rules, dict_stats):
    if rules is None:
        return 0, dict_stats #(0, 0)
    elif rules["name"] in dict_stats:
        return dict_stats[rules["name"]][0], dict_stats
    else :
        funct = get_funct_from_str(rules)

    df_funct = df_data.apply(funct, axis=1)
    stats = df_funct.sum()
    
    liste_id = set(df_data[df_funct]["Libellé"])
    dict_stats[rules["name"]] = (stats, liste_id if liste_id is not None else list())
    
    # print(stats)
    return stats, dict_stats

def get_funct_from_str(rules):
    if rules["type"] == "BORNE":
        funct = lambda x: rules["value_min"] <= x[rules["colonne"]] and  x[rules["colonne"]] < rules["value_max"]
    else: # cond
        funct = lambda x: str(x[rules["colonne"]]) == str(rules["value"])
    return funct


def filtrage_level_rules(list_rules, seuil=0.1):
    #fonction filtrage list (?)
    dict_comp_regles = defaultdict(list)
    
    level = 2
    rules_level_2 = [(ind_rules_lev, (ru, lift)) for ind_rules_lev, (ru, lift) in enumerate(list_rules) if len(ru) == level]
    
    for ind_rules_lev, (rule_lev_2, lift_curr) in rules_level_2:
        names_rules_curr = [(ru['name']) for ru in rule_lev_2]
        for ind, (rules_all, lift_all) in enumerate(list_rules):
            names_rules = [(ru['name']) for ru in rules_all]
            print(names_rules, ind_rules_lev, ind)
            if ind_rules_lev == ind:
                continue
            if len(set(names_rules_curr) & set(names_rules)) == level:
                if (abs(lift_curr-lift_all)/max(lift_curr, lift_all)) < seuil:
                    dict_comp_regles[ind_rules_lev].append({
                        "ind": ind, 
                        "diff": set(names_rules_curr) ^ set(names_rules),
                        "name_rules": names_rules_curr,
                        "name_comp": names_rules
                    })
    return dict_comp_regles


def get_cond_modalite(modalite):
    if modalite == "Bucket_1_n100_n15":
        funct_moda = lambda x: x[target] < -15 or x[target] is None
    elif modalite == "Bucket_2_n15_n5":
        funct_moda = lambda x: x[target] >= -15  and x[target] < -5 
    elif modalite == "Bucket_3_n5_p5":
        funct_moda = lambda x: x[target] >= -5 and x[target] < 5 
    elif modalite == "Bucket_4_p5_p15":
        funct_moda = lambda x: x[target] >= 5 and x[target] < 15
    elif modalite == "Bucket_4a_p5_p10":
        funct_moda = lambda x: x[target] >= 5 and x[target] < 10
    elif modalite == "Bucket_4b_p10_p15":
        funct_moda = lambda x: x[target] >= 10 and x[target] < 15
    elif modalite == "Bucket_5_p15_p100":
        funct_moda = lambda x: x[target] >= 15
    else:
        raise Exception(f"La modalité {modalite} n'est pas connue")
    return funct_moda


def create_df_modalite(df_data, modalite) :
    # à sauvegarder dans le csv result
 
    df_moda = df_data[df_data.apply(get_cond_modalite(modalite), axis=1)]
    # if modalite == "Bucket_1_n100_n15":
    #     df_moda = df_data[df_data[target].apply(lambda x: x < -15 )]
    # elif modalite == "Bucket_2_n15_n5":
    #     df_moda = df_data[df_data[target].apply(lambda x: x >= -15  and x < -5 )]
    # elif modalite == "Bucket_3_n5_p5":
    #     df_moda = df_data[df_data[target].apply(lambda x: x >= -5 and x < 5 )]
    # elif modalite == "Bucket_4_p5_p15":
    #     df_moda = df_data[df_data[target].apply(lambda x: x >= 5 and x < 15)]
    # elif modalite == "Bucket_5_p15_p100":
    #     df_moda = df_data[df_data[target].apply(lambda x: x > 15)]
    # else:
    #     raise Exception(f"La modalité {modalite} n'est pas connue")
    return df_moda


def postprocess(path_file, input_path):
    df_results = pd.read_csv(path_file, index_col=0)
    df_results['CRITERES'] = df_results['CRITERES'].apply(lambda x: x.strip())
    # df_results = df_results.head(200)
    df_results.sort_values("Lift", axis=0, ascending=False, 
                        inplace=True, na_position='last')
    df_results = df_results.reset_index()
    
    df_data = pd.read_csv(input_path)
    nb_total, _ = df_data.shape
    print("Lecture du input_path ", df_data.shape, "et regles ", df_results.shape)
    # print(list(df_results["POPULATION_TOTALE"])[0])
    # print(df_results.columns)
    for col in ["STATS.GENRE", "SITMIL.ARMEE", "SITMIL.CORPS", "SITMIL.GRADE", "_AFFECTATION.ZONE"]: #,"AFFECTATION.MICMZONEGEO"]:
        df_data[col] = df_data[col].apply(str) #, errors='ignore') #check si on ne peut pas le faire dans le read_csv

    df_data["CLAIR_ARMEE"] = df_data["SITMIL.ARMEE"]
    df_new = df_results.copy()
    
    rules_for_filtrage = list()
    rules = defaultdict(list)
    dict_stats = dict() # temp to save le calcul des stats des regles
    dict_stats_modalite = dict() # à reinitialiser par modalite
    # dict_set_lib = dict()
    # dict_set_lib_moda = dict()

    # duty !!!! plus nécessaire à partir des fichiers REGLES_***
    df_data_sub_1 = df_data[(df_data["SITMIL.GRADE"] != "AH") | \
                                    (df_data["_CELIBATAIRE_FAMILLE"].astype(str) != "CELIBATAIRE")]
    cond_part2 = (df_data["SITMIL.CORPS"] == "9310") | (df_data["SITMIL.CORPS"] == "9110") |\
                                ((df_data["AFFECTATION.AFFECTATION"] == "PARIS") & (df_data["AFFECTATION.LOGEMENTGRATUIT"]==False) 
                                 & (df_data["_NB_ENFANT_TOTAL_CF"] >= 2))
    df_regles_part2 = df_data[~(cond_part2)]

    cond_part3 = ((df_data["SITMIL.CORPS"] == "9204") & (df_data["SITMIL.GRADE"] == "5")) |\
                ((df_data["SITMIL.CORPS"] == "9204") & (df_data["SITMIL.GRADE"] == "DS")) |\
                ((df_data["SITMIL.GRADE"] == "GS") & (df_data["AFFECTATION.MICM"] == False)) |\
                ((df_data["SITMIL.ARMEE"] == "304") & (df_data["_CELIBATAIRE_FAMILLE"] == "FAMILLE") & (df_data["AFFECTATION.MICM"] == False)) 
    df_regles_part3 = df_data[~(cond_part3)]
    if list(df_results["POPULATION_TOTALE"])[0] == df_data_sub_1.shape[0]:
        df_data = df_data_sub_1
    elif list(df_results["POPULATION_TOTALE"])[0] == df_regles_part2.shape[0]:
        df_data = df_regles_part2
    elif list(df_results["POPULATION_TOTALE"])[0] == df_regles_part3.shape[0]:  
        df_data = df_regles_part3
    # !!!!

    modalite =  df_results.loc[0, "MODALITE"]
    df_modalite = create_df_modalite(df_data, modalite)
    print("Data de forme ", df_data.shape, df_modalite.shape, modalite)
    # print(df_modalite[df_modalite["SITMIL.CORPS"]=="9981"].shape)
    # print(df_modalite.groupby(["SITMIL.CORPS", "MODALITE"]).size())
    # a
    for ind, row in df_results.iterrows():
        name_regles = row["CRITERES"]
        rules_convert = split_convert_rules(name_regles)
        
        
        print("*"*5, ind, "*"*5, df_modalite.shape, row["Lift"], row["CRITERES"], row['POPULATION_TOTALE_MODALITE'])
        for ind_rule in range(0,3):
            rule = rules_convert[ind_rule] if len(rules_convert) > ind_rule else None
            stats, dict_stats = calcul_stats_rules(df_data, rule, dict_stats)
            stats_modalite, dict_stats_modalite = calcul_stats_rules(df_modalite, rule, dict_stats_modalite)
            rules["REGLES_"+ str(ind_rule+1)].append(rule["name"] if rule else None)
            rules["NB_INDIVIDUS_"+ str(ind_rule+1)].append(stats)
            rules["NB_INDIVIDUS_MODALITE_"+ str(ind_rule+1)].append(stats_modalite)
            rules["NB_INDIVIDUS_%_"+ str(ind_rule+1)].append(round(stats_modalite/stats *100,2) if stats_modalite > 0 else 0)
            print("\t [",rule["name"] if rule else None,"] ->",  stats, stats_modalite)
            
            # construction du dictionnaire qui contient le set des 3 règles            
            # if dict_stats is not None and  rule is not None and dict_stats[rule["name"]] is not None:
            #     if name_regles in dict_set_lib:
            #         dict_set_lib[name_regles].update(dict_stats[rule["name"]][1])
            #     else: 
            #         dict_set_lib[name_regles] = set((dict_stats[rule["name"]][1]))
            
            # if dict_stats_modalite is not None and  rule is not None and dict_stats_modalite[rule["name"]] is not None:
            #     if name_regles in dict_set_lib_moda:
            #         # print(name_regles, len(dict_stats_modalite[name_regles]), type(dict_stats_modalite[name_regles]), dict_stats_modalite[name_regles])
            #         dict_set_lib_moda[name_regles].update(dict_stats_modalite[rule["name"]][1])
            #     else: 
            #         dict_set_lib_moda[name_regles] = set((dict_stats_modalite[rule["name"]][1]))
            #         # print("ee", name_regles, len(dict_stats_modalite[name_regles]), type(dict_stats_modalite[name_regles]))
            
        rules_for_filtrage.append((rules_convert, row['Lift']))
    
    
    for key in rules:
        df_new[key] = rules[key]

    dict_filtrage = filtrage_level_rules(rules_for_filtrage)
    df_new['FILTRAGE'] = False
    df_new['FILTRAGE_CRITERE'] = None

    for key, infos in dict_filtrage.items():
        for inf in infos:
            # print("filtrage ", key, inf['ind'])
            df_new.loc[df_new.index[inf['ind']], 'FILTRAGE'] = key
        set_all_diff = {set_diff for inf in infos for set_diff in inf['diff']}
        set_all_str = ', '.join(str(e) for e in set_all_diff)
        df_new.loc[df_new.index[int(key)], 'FILTRAGE_CRITERE'] =  set_all_str

    df_new.to_csv(path_file.replace(".csv", "_formate.csv"))

    df_data["union_regles"] = False
    # à voir si il ne faut pas garder que les donnes sur la modalites
    # df_data["inter_regles"] = False
    for ind_regle, infos_fusion_regles in dict_filtrage.items():
        nom_regles = "reg_" + str(ind_regle) #infos_fusion_regles["name"]
        
        df_data[nom_regles] = False
        if len(infos_fusion_regles) > 0:
            name_rules = infos_fusion_regles[0]["name_rules"] # les fils sont toutes les déclinaisons

        print(nom_regles, name_rules)
        if len(name_rules) == 1:
            mega_funct = lambda x:(get_funct_from_str(get_rule(name_rules[0]))(x)) 
                # and (get_cond_modalite(modalite)(x))
        elif len(name_rules) == 2:
            mega_funct = lambda x:(get_funct_from_str(get_rule(name_rules[0]))(x)) and \
                                    (get_funct_from_str(get_rule(name_rules[1]))(x)) 
                                    # and \
                                    # (get_cond_modalite(modalite)(x))
        elif len(name_rules) == 3:
            mega_funct = lambda x:(get_funct_from_str(get_rule(name_rules[0]))(x)) and \
                                    (get_funct_from_str(get_rule(name_rules[1]))(x)) and \
                                    (get_funct_from_str(get_rule(name_rules[2]))(x)) 
                                    # and \
                                    # (get_cond_modalite(modalite)(x))

        # print(df_data[df_data.apply(mega_funct, axis=1)].shape)
        df_data.loc[df_data.apply(mega_funct, axis=1), nom_regles] = True
        df_data.loc[df_data.apply(mega_funct, axis=1) , "union_regles"] = True
        # print(df_data[df_data[nom_regles] == True].shape)
                #and (funct2) and (funct3))
        
        # for rules in infos_fusion_regles:
            # cond = df_data.apply(mega_funct, axis=1)
            # print(cond.sum())
            # df_data.loc[df_data.apply(mega_funct, axis=1), nom_regles] = True
    # df_data.to_csv("add_rules.csv")
    df_data.to_csv(path_file.replace(".csv", "_df_avec_regles_sans_mod.csv"), index=False)

    ############### Matrice diffsection #########################""""
    # df_without_rules = df_new[df_new['FILTRAGE']==False]
    # list_col = list(df_without_rules["CRITERES"]).insert(0, "NAME")
    # # list_col.insert(0, "NAME")
    # dict_matrice = dict()
    # for val in ("diff_modalite", "union_modalite", "diff_general", "union_general"):
    #     dict_matrice[val] = pd.DataFrame(columns=list_col)

    # for ind, row_1 in df_without_rules.iterrows():
    #     print("*"*5, row_1["CRITERES"], "*"*5)
    #     for key, df in dict_matrice.items():
    #         df.loc[ind, "NAME"] = row_1["CRITERES"]
    #     for _, row_2 in df_without_rules.iterrows():
    #         diff_general = len(dict_set_lib[row_1["CRITERES"]] ^ dict_set_lib[row_2["CRITERES"]])
    #         union_general = len(dict_set_lib[row_1["CRITERES"]] | dict_set_lib[row_2["CRITERES"]])

    #         diff_modalite = len(dict_set_lib_moda[row_1["CRITERES"]] ^ dict_set_lib_moda[row_2["CRITERES"]])
    #         union_modalite = len(dict_set_lib_moda[row_1["CRITERES"]] | dict_set_lib_moda[row_2["CRITERES"]])

    #         dict_matrice["diff_modalite"].loc[ind, row_2["CRITERES"]]= diff_modalite
    #         dict_matrice["union_modalite"].loc[ind, row_2["CRITERES"]]= union_modalite
    #         dict_matrice["diff_general"].loc[ind, row_2["CRITERES"]]= diff_general
    #         dict_matrice["union_general"].loc[ind, row_2["CRITERES"]]= union_general

    # for key, df in dict_matrice.items():
    #     df.to_csv(path_file.replace(".csv", "_" + key + "_id.csv"), index=False)

   


def main():
    parser = argparse.ArgumentParser(usage="Postprocess Results File from pysubgroup")
    parser.add_argument("--input", "-i", help="Dossier result postprocess from pysubgroup")
    parser.add_argument("--dataset", "-d", help="Dataset pour recalculer les stats")
    args = parser.parse_args()

    path_dataset = args.input.split("_Bucket")[0] + "_INPUT_MODALITE.csv"
    if os.path.exists(path_dataset):
        args.dataset = path_dataset

    print(args.input, args.dataset)
    postprocess(args.input, args.dataset)

if __name__ == "__main__":
    main()