import argparse
import pandas as pd 
import numpy as np

def calcule_grade_selon_foyer(df, stat_final, name="", col_grade="GRADE_SEMIS", col_foyer="Total personne à charge"):
    count = df.groupby([col_grade, col_foyer]).size()
    pond = (count/df.shape[0])*100
    
    if name != "DAF":
        df.loc[:, "COUT"] = df['GAINPERT_SSCS_S12CPL'].abs()
        total = df.groupby([col_grade, col_foyer])["COUT"].agg('sum')
        
        stat_pourcent = pd.DataFrame({'COUNT': count, 'POURCENT': pond, 'TOTAL': total}).reset_index()
        stat_pourcent.columns = ["GRADE" + name, "NBRE_FOYER" + name, "COUNT_" + name, "POURCENT_" + name, "TOTAL_GP_" + name]

    else: 
        stat_pourcent = pd.DataFrame({'COUNT': count, 'POURCENT': pond}).reset_index()
        stat_pourcent.columns = ["GRADE" + name, "NBRE_FOYER" + name, "COUNT_" + name, "POURCENT_" + name]

    
    stat_pourcent["CLE"] = stat_pourcent.apply(lambda x: x["GRADE" + name] + "_" + str(int(x["NBRE_FOYER" + name])), axis=1)

    if stat_final is None:
        df_merge = stat_pourcent
    else:    
        df_merge = pd.merge(stat_final, stat_pourcent.iloc[:, 2:], left_on='CLE', right_on='CLE', how='outer') 

    return df_merge


dict_correction = {   "Officers subalternes":"Officiers subalternes",
                      "officiers subalternes":"Officiers subalternes",
                      "officiers supérieurs":"Officiers supérieurs", 
                      "militaire du rang": "Militaire du rang",
                      "Sous offciers supérieurs": "Sous officiers supérieurs",
                      "Sous officiers supérieurs": "Sous Officiers supérieurs"
                      }

def calcul_duplication_terre(df_merge_formate, NBRE_IND_REFERENCE=113300):
    cond_terre = df_merge_formate["armee_x"] == "TERRE"
    df_merge_formate["Libellé_id"] = df_merge_formate["Libellé"].astype(str) 
    df_merge_formate["CLAIR_ARMEE_PROJ"] = df_merge_formate["CLAIR_ARMEE"]
    df_terre_brut = df_merge_formate[cond_terre]
    df_terre_brut.loc[:, "CLAIR_ARMEE_PROJ"]= df_terre_brut["CLAIR_ARMEE"] + "_BRUT" 
    
    brut_shape = df_terre_brut.shape
    df_terre_proj = pd.concat([df_terre_brut]*3, ignore_index=True)
    df_terre_proj["CLAIR_ARMEE_PROJ"] = df_terre_proj["CLAIR_ARMEE"] + "_PROJ" 

    nb_ind_remove = (df_terre_proj.shape[0] + df_terre_brut.shape[0]) - NBRE_IND_REFERENCE #  cible_terre  =119 693 
    np.random.seed(10)
    drop_indices = np.random.choice(df_terre_proj.index, nb_ind_remove, replace=False)
    df_shape = df_terre_proj.shape
    df_terre_proj = df_terre_proj.drop(drop_indices)

    df_terre_sur_ech = pd.concat([df_terre_proj, df_terre_brut], ignore_index=True)

    # print(f"Remove {nb_ind_remove}")
    # print(f"{brut_shape, df_terre_proj.shape, df_shape, df_terre_sur_ech.shape}")
    df_terre_sur_ech["Libellé_id"] = df_terre_sur_ech.index.astype('str')  + "_" + df_terre_sur_ech["Libellé"].astype(str) 
    

    df_sans_terre = df_merge_formate[df_merge_formate['armee_x'] != "TERRE"]
    df_sur_ech = pd.concat([df_terre_sur_ech, df_sans_terre] , ignore_index=True, sort=False)
   
    return df_sur_ech


def calcul_projection_terre(df_semis, coeff_ponderation, NBRE_IND_REFERENCE=113300, without_bspp=True, armee_a_pnderer="Armee de Terre"):
    np.random.seed(0)
    # Projection selon les proportions de la DAF pour la population Terre

    cond_bspp = (df_semis["SITMIL.CORPS"] == 9164) | (df_semis["SITMIL.CORPS"] == 9969)
   
    if without_bspp :
        df_semis.loc[cond_bspp, 'CLAIR_ARMEE_PROJ'] = "BSPP" 
    df_semis["Libellé_id"] = df_semis["Libellé"].astype(str) 
    
    cond_terre = (df_semis["CLAIR_ARMEE_PROJ"] == armee_a_pnderer)

    df_semis_terre = df_semis[cond_terre] 
    df_sans_terre = df_semis[~(cond_terre)]
    
    df_semis_terre.reset_index(inplace=True)
    df_semis_terre.loc[:, "cle"] = df_semis_terre.apply(lambda x: x["INFO_CATEGORIE"] + "_" + str(int(x["INFO_NBREFOYER"])), axis=1)
    df_terre_brut = df_semis_terre.copy()

    count_pop = df_semis_terre["cle"].value_counts().to_dict()
    df_semis_terre.loc[:,"proj_pond"] = df_semis_terre.apply(lambda x: 
            coeff_ponderation['count'].get(x['cle'], 'error'), axis=1)
    df_semis_terre.to_csv("df_semis_terre.csv")

    df_semis_terre.loc[:,"proj_pond"] = df_semis_terre.apply(lambda x: 
            coeff_ponderation['count'].get(x['cle'], 0)/(count_pop[x['cle']]), axis=1)
    
    pouieme = df_semis_terre["proj_pond"].sum() 
    _pouieme_rep = (1.0 - pouieme) / df_semis_terre.shape[0] # des pouièmes  
    df_semis_terre.loc[:, "proj_pond"] = df_semis_terre["proj_pond"] + _pouieme_rep

    df_semis_terre.loc[:, "CLAIR_ARMEE_PROJ"] = df_semis_terre["CLAIR_ARMEE_PROJ"] + "_PROJ" 

    nb_inidividus_terre = NBRE_IND_REFERENCE - df_terre_brut.shape[0]
    
    indices = np.random.choice(df_semis_terre.index, nb_inidividus_terre, replace=True, p=df_semis_terre["proj_pond"])

    df_proj = df_semis_terre.iloc[indices, :]
    df_proj.loc[:, "Libellé_id"] = df_proj.index.astype('str')  + "_" + df_proj["Libellé"].astype(str) 
 
    df_terre_brut.loc[:, "CLAIR_ARMEE_PROJ"] = df_terre_brut["CLAIR_ARMEE_PROJ"] + "_BRUT" 
    df_terre_sur_ech = pd.concat([df_terre_brut, df_proj], ignore_index=True, sort=False)
    df_sur_ech = pd.concat([df_terre_sur_ech, df_sans_terre] , ignore_index=True, sort=False)

    return df_sur_ech

def calcul_ponderation_DAF():
    df_daf = pd.read_csv(r"..\Fichiers\DAF\20180913_synthèse_demande 28_VDEF.csv", 
                            encoding="latin1", delimiter=",")
    df_daf.columns = [col.replace("\n", "") for col in df_daf.columns]

    df_grade = pd.read_csv(r"..\Fichiers\DAF\Grade_GroupeGrade_DAF.csv",
                            delimiter=",",)
    df_grade = df_grade[['GRADE', 'GROUPE GRADE']]
    #GRADE, GROUPE_GRADE

    print(df_grade.head())
    print(df_daf.head())

    dict_grade = df_grade.set_index('GRADE').T.to_dict('str')
    df_daf["GRADE_SEMIS"] = df_daf["GRADE"].apply(lambda x: dict_grade[x])
    df_daf["Zonage"] = df_daf["Zonage"].str.strip()

    print("armee DAF \n", df_daf["ARMEES"].value_counts())

    df_daf_terre = df_daf[df_daf["ARMEES"]=="ADT"]
    pond = df_daf_terre.groupby(["GRADE_SEMIS", "Total personne à charge"]).size()/df_daf_terre.shape[0]

    stat_pourcent= dict()
    stat_pourcent = calcule_grade_selon_foyer(df_daf_terre, None, "DAF","GRADE_SEMIS", "Total personne à charge")

    print("Nb Ind DAF Terre", df_daf_terre.shape[0])
    print("zonage", df_daf_terre.groupby("Zonage").size()/df_daf_terre.shape[0])
    print(pond)

    df_pond = pd.DataFrame({'count': pond}).reset_index()
    df_pond["cle"] = df_pond.apply(lambda x: x["GRADE_SEMIS"] + "_" + str(int(x["Total personne à charge"])), axis=1)
    df_dict_pond= df_pond[["count", "cle"]]
    df_dict_pond.to_csv(r"..\Fichiers\DAF\Ponderation_grade_population_DAF.csv")

    df_pond = pd.read_csv(r"..\Fichiers\DAF\Ponderation_grade_population_DAF.csv")
    coeff_ponderation = df_pond.set_index("cle").T.to_dict('int')
    print(coeff_ponderation['count']['Sous Officiers supérieurs_10'])
    print(set(coeff_ponderation['count'].keys()))


def calcule_ponderation(path, nombre_ind_cible_TERRE, nombre_ind_cible_BSPP):
    df_pond = pd.read_csv(r"..\Fichiers\DAF\Ponderation_grade_population_DAF.csv")
    coeff_ponderation = df_pond.set_index("cle").T.to_dict('int')

    df_semis = pd.read_csv(path, delimiter=",")
    for before, after in dict_correction.items():
            df_semis['INFO_CATEGORIE'] = df_semis['INFO_CATEGORIE'].apply(lambda x: x.replace(before, after))

    df_semis["CLAIR_ARMEE_PROJ"] = df_semis["CLAIR_ARMEE"]
    df_sur_ech = calcul_projection_terre(df_semis, coeff_ponderation, NBRE_IND_REFERENCE=nombre_ind_cible_TERRE)
    print("CLAIR_ARMEE_PROJ \n", df_sur_ech["CLAIR_ARMEE_PROJ"].value_counts())

    df_sur_ech = calcul_projection_terre(df_sur_ech, coeff_ponderation, NBRE_IND_REFERENCE=nombre_ind_cible_BSPP, armee_a_pnderer="BSPP")
    df_sur_ech.to_csv(path.replace(".csv", "_PROJ_POND_"+ str(nombre_ind_cible_TERRE)+".csv"), index=False)
    print("CLAIR_ARMEE_PROJ\n", df_sur_ech["CLAIR_ARMEE_PROJ"].value_counts())


def main():
    parser = argparse.ArgumentParser(usage="")
    parser.add_argument("--path", "-p", help="Fichier à échantillonner")
    parser.add_argument("--nb_ind_terre", "-n", help="Nombre cible à projeter", default=112600)
    parser.add_argument("--nb_ind_bspp", help="Nombre cible à projeter", default=8500)

    args = parser.parse_args()
    calcule_ponderation(args.path, args.nb_ind_terre, args.nb_ind_bspp)


if __name__ == "__main__":
    main()
