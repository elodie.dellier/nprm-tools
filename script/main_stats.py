from pathlib import Path
import pandas as pd
import streamlit as st
import plotly_express as px
import argparse 

@st.cache
def load_data(data_path):
#    data_path = Path() / 'data/SCA_6_nprm.xlsx'
#    data = pd.read_excel(data_path, header=1, skiprows=1)
    data = pd.read_csv(data_path)
    return data


def calcule_percent(df, nom_col_new,  col_v1, col_v2, col_gain_perte):
    df['TOTAL_' + nom_col_new] = df[col_gain_perte]
    df['TOTAL_%_' + nom_col_new] = round(((df[col_v2] - df[col_v1]) /df[col_v1]) *100, 2)
    df['GAIN_%_' + nom_col_new] = df['TOTAL_%_' + nom_col_new].apply(lambda x: x if x > 0 else 0)
    df['PERTE_%_' + nom_col_new] = df['TOTAL_%_' + nom_col_new].apply(lambda x: x if x < 0 else 0)
    return df, ['TOTAL_' + nom_col_new, 'TOTAL_%_' + nom_col_new, 'GAIN_%_' + nom_col_new, 'PERTE_%_' + nom_col_new]


def preprocess_data(df):
    #  1250 -1200/1200 x 100 
    df['TOTAL'] = df['GAINPERTE_TTBLC_6']
    df['TOTAL_%'] = ((df['TTLGE_ACPLAFDT20ISOC'] - df['TOTAL_GENERAL_V1']) /df['TOTAL_GENERAL_V1']) *100
    df['GAIN_%'] = df['TOTAL_%'].apply(lambda x: x if x > 0 else 0)
    df['PERTE_%'] = df['TOTAL_%'].apply(lambda x: x if x < 0 else 0)
    return df
    

def preprocess_data_commun(df):
    df['CELIBATAIRE/FAMILLE'] =  ((df['NBRE_ENF_CF'] > 0) |  (df['SITFAM'] == "M") | (df['SITFAM'] == 'P') | (df['SITFAM'] == 'S')).apply(
                                lambda x: 'FAMILLE' if x  else 'CELIBATAIRE')
    return df


def get_stat(df, col='GAIN_INDICIAIRE', name_stat_desc='Général'):
    stats ={}
    stats['Total'] = len(df) 
    df = df[df[col] != 0]
    # stats['Nom'] = name_stat_desc 
    stats['Nombre G/P'] = len(df) 
    stats['Moyenne'] = df[col].mean()
    stats['Médiane'] = df[col].median()
    stats['Minimum'] = df[col].min()
    stats['Maximum'] = df[col].max()
    return stats


def get_stats_group(df, group_by='INFO_GENRE', col='GAIN_INDICIAIRE'):
    df_group = df.groupby([group_by])
    for name, group in df_group:
        print(name, len(group))
        print(get_stat(group, col))
    stats_by_group = {name : get_stat(group, col) for name, group in df_group}
    return stats_by_group


def get_affichage_stat( df_pre, group_by='INFO_GENRE', cols=['TOTAL_INDICIAIRE', 'TOTAL_INDICIAIRE_%', 'GAIN_INDICIAIRE_%', 'PERTE_INDICIAIRE_%']):
    stats_par_genre = {}
    for key in cols:
        stats_par_genre_group = get_stats_group(df_pre, group_by, key)
        if len(stats_par_genre) == 0:
            for genre in stats_par_genre_group:
                stats_par_genre[genre] = {}
        for genre in stats_par_genre_group:
            stats_par_genre[genre][key] =  stats_par_genre_group[genre]
    return stats_par_genre


def main(input):
    df = load_data(input)
    df = df[df["_merge"] == "both"]
    # df_pre = preprocess_data(df.copy())
    df_pre = preprocess_data_commun(df.copy())
    df_pre, list_indiciaire = calcule_percent(df_pre, "INDICIAIRE", 'SOLBAS', 'SOLBASV2', 'GAIN_INDICIAIRE')
    df_pre, list_etat_mil= calcule_percent(df_pre, "ETAT_MIL", 'V1_TOTAL_GARNISON', 'TTL_BLCETATMIL_GARV2', 'GAINPERT_ETATMIL_GAR')
    df_pre, list_total= calcule_percent(df_pre, "TOTAL", 'TOTAL_GENERAL_V1', 'TTLGE_ACPLAFDT20ISOC', 'GAINPERTE_TTBLC_6')
    
    # st.dataframe(df_pre, width=1500) #.style.highlight_max(axis=0))

    list_col_compute = dict()
    list_col_compute['Indiciaire'] = list_indiciaire #['TOTAL_INDICIAIRE', 'TOTAL_INDICIAIRE_%', 'GAIN_INDICIAIRE_%', 'PERTE_INDICIAIRE_%']
    list_col_compute['Etat militaire'] = list_etat_mil
    # list_col_compute['Qualification'] = list_total
    list_col_compute['Total']  = list_total # ['TOTAL', 'TOTAL_%', 'GAIN_%', 'PERTE_%']

    option = st.selectbox(
    'Quel bloc souhaitez-vous obtenir les statistiques?',
    list(list_col_compute.keys()))

    list_col_compute = list_col_compute[option] 

    stats_commun = {}
    stats_commun['Nb de lignes'] = len(df.index)
    stats_commun['Nb Gagnants '] = int(df_pre[list_col_compute[1]].astype(bool).sum(axis=0)) 
    stats_commun['Nb Perdants '] = int(df_pre[list_col_compute[2]].astype(bool).sum(axis=0))
    stats_commun['Coût scénario'] =  round(df_pre[list_col_compute[0]].sum(), 3)


    stats_general = {}
    for key in list_col_compute:
        stats_general[key] =  get_stat(df_pre, key)

    stats_par_genre = get_affichage_stat(df_pre, 'INFO_GENRE', list_col_compute)
    stats_par_famille = get_affichage_stat(df_pre, 'CELIBATAIRE/FAMILLE', list_col_compute)
    stats_par_armee = get_affichage_stat(df_pre, 'SITMIL.ARMEE' , list_col_compute) #CLAIR_ARMEE'
    stats_par_affectation = get_affichage_stat(df_pre, 'AFFECTATION.AFFECTATION', list_col_compute)

    # df.pivot_table(index = 'Nom', columns = 'T', values = 'V')
    st.markdown('# Statistiques Globales ' + option)
    st.write(stats_commun)
    st.dataframe(pd.DataFrame(stats_general).T)
    st.markdown('# Statistiques selon le genre')
    for key in stats_par_genre:
        st.markdown(f'## {key}')
        st.dataframe(pd.DataFrame(stats_par_genre[key]).T)
    st.markdown('# Statistiques selon la situation familiale')

    for key in stats_par_famille:
        st.markdown(f'## {key}')
        st.dataframe(pd.DataFrame(stats_par_famille[key]).T)

    st.markdown('# Statistiques selon l"armee')
    for key in stats_par_armee:
        st.markdown(f'## {key}')
        st.dataframe(pd.DataFrame(stats_par_armee[key]).T)

    st.markdown('# Statistiques selon l"affectation')
    for key in stats_par_affectation:
        st.markdown(f'## {key}')
        st.dataframe(pd.DataFrame(stats_par_affectation[key]).T)

    df_pre.to_csv("dataframe_statistique.csv")
    df_desc = pd.DataFrame(df_pre, columns=list_col_compute)
    # print(df.describe())

    st.markdown(f'# Description')
    st.write(df_desc.describe())
    # mean_counts_by_hour = pd.DataFrame(df_pre.groupby(['hour', 'season'], sort=True)['count'].mean()).reset_index()
    fig1 = px.histogram(df_pre, x=list_col_compute[1])
    st.write(fig1)

    # fig_all = px.histogram(df_pre, x=list_col_compute[1])

if __name__ == "__main__":
    parser = argparse.ArgumentParser(usage="merge -i path_semis_fusion")
    parser.add_argument("--input", "-i", help="")
    args = parser.parse_args()
    main(args.input)




