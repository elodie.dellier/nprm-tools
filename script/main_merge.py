import argparse
from pathlib import Path
import pprint
import pandas as pd
from collections import defaultdict
from datetime import datetime
import os 
import numpy as np

list_armee = ["AIR", "MARINE", "TERRE",  "SCA", "SEA", "GND", "DGA", "SID", "SSA"]

new_to_old_columns = {
    "EM_V1" : ["V1_TOTAL_GARNISON"],
    "EM_V2" : ["TTL_ETATMILGAR_S06", "TTLBLCETATMIL_S12_CP"],
    "EM_GAIN_PERTE" : ["GAINPERTETAMILGAR_S6", "GAINPERTETAMILGAS12", "GPERTETAMILGAR_S6", "GAINPERTE_S12_CPL_MI"],

    "TOTAL_V1": ["TOTAL_GENERAL_V1SSCS"],
    "TOTAL_V2":  ["TTLGE_S12_SSCS_CPLE", "TTLGE_S6_SS_CS_CPLE"],
    "TOTAL_GAIN_PERTE": ["GAINPERT_SSCS_S12CPL", "GAINPERTE_S06_SS_CS", "GAPERTE_S06_SS_CS"],

    "TOTAL_V1_AVEC_CS": ["TOTAL_GENERAL_V1_S6T"],
    "TOTAL_V2_AVEC_CS":  ["V2_TTLGE_S6TER_SSPCT"],
    "TOTAL_GAIN_PERTE_AVEC_CS": ["GAINPERTE_TTLGE_S6TE"],

    "PP_V2":["V2_TTLQAL_ACPLAFDT20"],
    "PP_V1": ["V1_TOTALQUAL"],
    "PP_GAIN_PERTE": ["GAINPERQALSEUALPLA20"],

    "CS_V2": ["V2_TTLEBLC_CS_S06TER"],
    "CS_V1": ["V1_TTLEMPLOISPEC_S6T"],
    "CS_GAIN_PERTE": ["GAINPERT_CS_S6TER"]
}


def def_zone(x):
    if x == 0 or x == 4:
        return "0_4"
    elif x == 1 or x == 2 or x == 3:
        return "1_2_3"
    else:
        return None
         
def fill_nan(df, colonne, valeur=0):
    df[colonne].fillna(valeur, inplace=True)

# preprocess et créer un fichier formaté/brut 
def preprocess(df):
    liste_fill_nan_par_0 = ["SITFAM.SITFAM_ENF_CFTX1", "SITFAM.SITFAM_ENF_CFTX2", "SITFAM.SITFAM_ENF_CFTX3", "SITFAM.SITFAM_ENF_CFTX4", "SITFAM.SITFAM_ENF_CFTX5", \
                            "SITMIL.B2_GENDARME", "SITMIL.ANC_SERVICE", "MOBILITE.LOGEGRATUIT", "AFFECTATION.HOPITAL", "AFFECTATION.LOGEMENTGRATUIT",\
                            "AFFECTATION.DROMCOM", "MOBILITE.NBMUT", "SITFAM.SITFAM_ENF_SFSTX1", "SITFAM.SITFAM_ENF_SFSTX2", "SITFAM.SITFAM_ENF_SFSTX3", 
                            "AFFECTATION.MICM", "AFFECTATION.MICMLOYER", "AFFECTATION.MICMAIDE", "AFFECTATION.MICMLOGDEF", "RESP_ET_PERF.NBI",
                            "EMPSPEC.PLONGE", "EMPSPEC.ISA", "EMPSPEC.NEDEX", "EMPSPEC.MAERO", "EMPSPEC.PFEU","EMPSPEC.SECCIV",
                            "EMPSPEC.ISSA", "EMPSPEC.SMA","EMPSPEC.EMBQ","EMPSPEC.ATOM", "ENGOPS.CSCHMI", "ENGOPS.AOPER",
                            "ENGOPS.ISAPB_NBJOURS", "ENGOPS.CAMP", "ENGOPS.OPEX", "QUALIFICATION_MIL.RESPPETRO", "QUALIFICATION_MIL.QAL54_2ANS",
                            "QUALIFICATION_MIL.PQUALOFF", "QUALIFICATION_MIL.AUTREQAL54", "QUALIFICATION_MIL.PQUALSOFF", "QUALIFICATION_MIL.PHT", 
                            "QUALIFICATION_MIL.PRIME_SOFF", "QUALIFICATION_MIL.QAL_54_DT","QUALIFICATION_MIL.PTAMP", 
                            "V2_ACT_SPEC.V1_ACT_AERIENNE", "V2_ACT_SPEC.V1_ACT_AEROPORTE",
                            "V2_ACT_SPEC.V1_BAT_SURFACE", "V2_ACT_SPEC.V1_ACT_SUBAQUATIQUE",
                            "V2_ACT_SPEC.V_ACT_MONTAGNE", "V2_ACT_SPEC.V1_ACT_HUM_SUBAQUATIQUE",
                            "V2_ACT_SPEC.V1_ACT_NEUT_EXPLOSIF", "V2_ACT_SPEC.V1_NRBC",
                            "V2_ACT_SPEC.V1_ACT_MAERO", "V2_ACT_SPEC.V1_LINGUISTE_ECOUT",
                            "V2_ACT_SPEC.V1_ACT_RESPO", "V2_ACT_SPEC.V1_ACT_SECU_AERIENNE",
                            "V2_ACT_SPEC.V1_SECPERSONNES"
                            ]
                       
    for col in liste_fill_nan_par_0:
        fill_nan(df, col, 0)
        df[col] = df[col].apply(pd.to_numeric, errors='coerce')

    _SITFAM_SITFAM_ENF_CFTX = df["SITFAM.SITFAM_ENF_CFTX1"] + df["SITFAM.SITFAM_ENF_CFTX2"] + df["SITFAM.SITFAM_ENF_CFTX3"] + df["SITFAM.SITFAM_ENF_CFTX4"] + df["SITFAM.SITFAM_ENF_CFTX5"]
    _SITFAM_SITFAM_ENF_SFSTX = df["SITFAM.SITFAM_ENF_SFSTX1"] + df["SITFAM.SITFAM_ENF_SFSTX2"]+ df["SITFAM.SITFAM_ENF_SFSTX3"] 
    df["_SITFAM.SITFAM_ENF"] = _SITFAM_SITFAM_ENF_CFTX #pd.concat([_SITFAM_SITFAM_ENF_CFTX, _SITFAM_SITFAM_ENF_SFSTX], axis=1).max(axis=1) 
    df["_CELIBATAIRE_FAMILLE"] =  ((_SITFAM_SITFAM_ENF_CFTX > 0) |  
                                  (df['SITFAM.SITFAM'] == "M") | 
                                  (df['SITFAM.SITFAM'] == 'P') | 
                                  (df['SITFAM.SITFAM'] == 'S')).apply(
                                        lambda x: 'FAMILLE' if x  else 'CELIBATAIRE')

    df["_NB_ENFANT_TOTAL_CF"] = _SITFAM_SITFAM_ENF_CFTX
    df["_NB_ENFANT_TOTAL_SFS"] = _SITFAM_SITFAM_ENF_SFSTX
    df['_NB_ENFANT_TOTAL_CF_BUCKET'] = _SITFAM_SITFAM_ENF_CFTX.apply(lambda x: '0' if x == 0 else ("4_Inf" if x>= 4 else "1_3"))

    df["STATS.COUPLE_MILITAIRE_ORI"]  = df["STATS.COUPLE_MILITAIRE"] 
    df["STATS.COUPLE_MILITAIRE"] = (df['STATS.COUPLE_MILITAIRE'] > 1).apply(lambda x: True if x else False) 
    df["_AFFECTATION.ZONE"] = df["AFFECTATION.ZONE"].apply(lambda x: def_zone(x)) 
    df["_AFFECTATION.MICMAIDE"] = df["AFFECTATION.MICMAIDE"].apply(lambda x: True if  float(x) > 0 else False)
    df["_DIVERS_INFIRMIER"] =  ((df["DIVERS.QUALMITHA"]>0) | (df["DIVERS.DIPSSA"]>0) | (df["DIVERS.TXDIPSSA"]>0) | \
                                (df["DIVERS.BMPM_UIISC_SSA"]>0) | (df["DIVERS.FORFMITHA"]>0)).apply(
                                    lambda x: True if float(x) >0  else False)


    df["_ACTIVITE_SPECIFIQUE_V1"] = ((df["EMPSPEC.PLONGE"]>0) | (df["EMPSPEC.ISA"]>0) | (df["EMPSPEC.NEDEX"]>0) | (df["EMPSPEC.MAERO"]>0) \
                                | (df["EMPSPEC.PFEU"]>0) | (df["EMPSPEC.SECCIV"]>0) | (df["EMPSPEC.ISSA"]>0)| (df["EMPSPEC.SMA"]>0.0) \
                                | (df["EMPSPEC.EMBQ"]>0) | (df["EMPSPEC.ATOM"]>0))

        
    df["_ENGAGEMENT_OPS_V1"] = ((df["ENGOPS.CSCHMI"]>0) | (df["ENGOPS.AOPER"]>0) | (df["ENGOPS.ISAPB_NBJOURS"]>0) \
                                | (df["ENGOPS.CAMP"]>0) | (df["ENGOPS.OPEX"]>0))
                          
    df["_QUALIFICATION_OUI_NON"] = ((df["QUALIFICATION_MIL.RESPPETRO"]>0) | (df["QUALIFICATION_MIL.QAL54_2ANS"]>0) | (df["QUALIFICATION_MIL.PQUALOFF"]>0) \
                                | (df["QUALIFICATION_MIL.AUTREQAL54"]>0) | (df["QUALIFICATION_MIL.PQUALSOFF"]>0) | (df["QUALIFICATION_MIL.PHT"]>0) \
                                | (df["QUALIFICATION_MIL.PRIME_SOFF"]>0) | (df["QUALIFICATION_MIL.QAL_54_DT"]>0) | (df["QUALIFICATION_MIL.PTAMP"]>0) 
                                )

    df["_ACTIVITE_SPECIFIQUE_V2"] = ((df["V2_ACT_SPEC.V1_ACT_AERIENNE"]>0)  | (df["V2_ACT_SPEC.V1_ACT_AEROPORTE"]>0) \
                                | (df["V2_ACT_SPEC.V1_BAT_SURFACE"]>0) | (df["V2_ACT_SPEC.V1_ACT_SUBAQUATIQUE"]>0) \
                                | (df["V2_ACT_SPEC.V_ACT_MONTAGNE"]>0) | (df["V2_ACT_SPEC.V1_ACT_HUM_SUBAQUATIQUE"]>0)\
                                | (df["V2_ACT_SPEC.V1_ACT_NEUT_EXPLOSIF"]>0)| (df["V2_ACT_SPEC.V1_NRBC"]>0)| (df["V2_ACT_SPEC.V1_ACT_MAERO"]>0)\
                                | (df["V2_ACT_SPEC.V1_LINGUISTE_ECOUT"]>0) | (df["V2_ACT_SPEC.V1_ACT_RESPO"]>0) \
                                | (df["V2_ACT_SPEC.V1_ACT_SECU_AERIENNE"]>0) | (df["V2_ACT_SPEC.V1_SECPERSONNES"]>0)
                                )
    
    df["SITMIL.ANC_SERVICE"] = df["SITMIL.ANC_SERVICE"].apply(lambda x : min(x, 45))
    df["AFFECTATION.AFFECTATION"] = df["AFFECTATION.AFFECTATION"].replace("PROV", "PROVINCE")

    # pour convertir les 1/0 en booleen
    for col in ["SITMIL.BREVET_MILITAIRE", "SITMIL.B2_GENDARME", "AFFECTATION.HOPITAL",
                "AFFECTATION.LOGEMENTGRATUIT", "MOBILITE.LOGEGRATUIT",
                "AFFECTATION.MICM", "AFFECTATION.MICMLOGDEF"]:
        df[col] = df[col].apply(bool)

    # pour que les données ne soient pas prises comme des valeurs mais comme des catégories
    for col in ["STATS.GENRE", "SITMIL.ARMEE", "SITMIL.CORPS", "SITMIL.GRADE", "AFFECTATION.ZONE","AFFECTATION.MICMZONEGEO"]:
        df[col] = df[col].apply(str)

    # nouvelle colonne 18032020
    df["TOUCHE_LA_MICM"] = (df["MICM"] > 0) 
    df["TOUCHE_GARNISON_ISOLEE"] = (df["PART_LOGT_ISO_SEUL"] > 0)  #prime_camp
    df["INFO_COUPLE_MILI"].fillna(0, inplace=True)
    df["B4_ACQUISE"] = df.apply(lambda x: b4_acquise(x), axis=1)
    return df


def b4_acquise(x):
    ### description qui provient de Stéphanie ###
#     Pour la colonne SITMIL.BREVET_MILITAIRE   elle doit être combiner avec la colonne » QUALIFICATION_MIL.PHT, 
#     alors si il y a une valeur dans une des deux colonnes c’est que la balise B4 est acquise pour les FAFR suivante : Marine, ADT, SEA, AIR.

# Pour le SSA il ne faut prendre que la colonne QUALIFICATION_MIL.PHT qui est la B4
# Pour la gendarmerie ne prendre que la PHT également . car attention la colonne SITMIL.BREVET_MILITAIRE   pour la gendarmerie sert pour la B3.
    if x["SITMIL.ARMEE"] in ["302", "303", "304", "309"]:  # Marine, ADT, SEA, AIR
        return ((x["SITMIL.BREVET_MILITAIRE"] > 0) | (x["QUALIFICATION_MIL.PHT"] > 0))
    elif x["SITMIL.ARMEE"] in ["305", "307"]: # Gendarmerie, SSA
        return (x["QUALIFICATION_MIL.PHT"] > 0)
    else:
        return False


def calcule_percent(df, nom_col_new,  col_v1, col_gain_perte):
    df[col_gain_perte] = df[col_gain_perte].apply(pd.to_numeric, errors='coerce')
    df[col_v1] = df[col_v1].apply(pd.to_numeric, errors='coerce')

    df['GAIN-PERTE_' + nom_col_new] = df[col_gain_perte]
    df['GAIN-PERTE_%_' + nom_col_new] = (df[col_gain_perte].astype(float) /(df[col_v1].astype(float))) *100
    df['GAIN-PERTE_%_' + nom_col_new+ '_ROUNDED'] = df['GAIN-PERTE_%_' + nom_col_new].round(decimals=2)
    return df  #, ['GAIN-PERTE_' + nom_col_new, 'GAIN-PERTE_%_' + nom_col_new, 'GAIN-PERTE_%_' + nom_col_new+ '_ROUNDED']


def categorise_gain_perte(x):
    if x is None :
        return "ERREUR"
    elif x > 1 :
        return "GAGNANTS"
    elif (x >= 0 and x <= 1):
        return "NEUTRES"
    else:
        return "PERDANTS"


def calcule_gain_total_sans_cs(df): 
    df['TOTAL_V1'] = df['EM_V1'] + df['PP_V1'] + df['SOLBAS'] 
    df['TOTAL_V2'] = df['EM_V2'] + df['PP_V2'] + df['SOLBASV2'] 
    df['TOTAL_GAIN_PERTE'] = df['TOTAL_V2'] - df['TOTAL_V1']
    return df


def calcule_gain_perte(df):
    #
    print([str(col) for col in df.columns])
    if 'TOTAL_GAIN_PERTE_AVEC_CS' in df.columns :

        df = calcule_gain_total_sans_cs(df)
        df = calcule_percent(df, "TOTAL_AVEC_CS", 'TOTAL_V1_AVEC_CS', 'TOTAL_GAIN_PERTE_AVEC_CS') 
        df = calcule_percent(df, "COMP_SPECIFIQUES", 'CS_V1', 'CS_GAIN_PERTE')

        print([str(col) for col in df.columns])
        df['GAGNANTS_PERDANTS_AVEC_CS'] = df['GAIN-PERTE_%_TOTAL_AVEC_CS'].apply(lambda x: categorise_gain_perte(x))
        df['COMPENSATRICE_AVEC_CS'] = df.apply(lambda x: abs(x['GAIN-PERTE_TOTAL_AVEC_CS']) if x['GAGNANTS_PERDANTS_AVEC_CS'] == "PERDANTS" else 0, axis=1) 
        df['COUT_DU_SCENARIO_AVEC_CS'] = df.apply(lambda x: x['GAIN-PERTE_TOTAL_AVEC_CS'] if x['GAIN-PERTE_TOTAL_AVEC_CS'] > 0 else 0, axis=1) 
        
    df = calcule_percent(df, "TOTAL", 'TOTAL_V1', 'TOTAL_GAIN_PERTE') #TOTALGE_V2_SCENAR12, GAINPERTEGENERAL_S12 'TOTALGE_V2_SCENAR12',
    df = calcule_percent(df, "INDICIARE", 'SOLBAS', 'GAIN_INDICIAIRE')
    df = calcule_percent(df, "ETAT_MILITAIRE-GARNISON", 'EM_V1', 'EM_GAIN_PERTE')
    df = calcule_percent(df, "PARCOURS-PRO", 'PP_V1', 'PP_GAIN_PERTE')
    

    df['GAGNANTS_PERDANTS'] = df['GAIN-PERTE_%_TOTAL'].apply(lambda x: categorise_gain_perte(x))
    df['COMPENSATRICE'] = df.apply(lambda x: abs(x['GAIN-PERTE_TOTAL']) if x['GAGNANTS_PERDANTS'] == "PERDANTS" else 0, axis=1) 
    df['COUT_DU_SCENARIO'] = df.apply(lambda x: x['GAIN-PERTE_TOTAL'] if x['GAIN-PERTE_TOTAL'] > 0 else 0, axis=1) 

    
        
    return df


def remove_data(df):
    # Suppression des IDs (Libellé ou Type) qui ne sont pas mergé
    df_without_id = df[df["_merge"] != "both"]
    print(" Suppression " + str(df_without_id.shape[0]) +" lignes sans id correspondant: \n", df_without_id[['Libellé', 'Types']].head(5))
    df = df[df["_merge"] == "both"]  # on ne garde que lles lignes qui ont un identifiant dans les deux fichiers
    print("\n")

    # Suppresion des lignes avec un genre vide
    df_without_sexe = df[df["STATS.GENRE"].isna()]
    print(" Suppression " + str(df_without_sexe.shape[0]) +" lignes avec un genre vide: \n")
    with open("diff_genre.txt", 'w') as fic:
        for ind, data in df_without_sexe.iterrows():
            fic.write(str(data["SITMIL.ARMEE"]) + "; "+ data['Libellé'] + "\n")
    df = df[df["STATS.GENRE"].notna()]  # on ne garde que lles lignes qui ont un genre valide (1 ou 2)
    print("\n")

    # Suppresion des lignes avec une situation familiale differente de M,C,N,P,D,V,S
    df_sit_fam = df['SITFAM.SITFAM'].apply(lambda x: x in ["M", "N", "P", "C", "S", "V", "D"]) 
    df_sit_fam_inc = df[df_sit_fam == False]
    print(" Suppression " + str(df_sit_fam_inc.shape[0]) +" lignes avec une situation vide: \n", df_sit_fam_inc[['Libellé', 'SITFAM.SITFAM']].head(5))
    df = df[df_sit_fam]
    print("\n")

    df['AFFECTATION.MICMAIDE'] = df['AFFECTATION.MICMAIDE'].apply(pd.to_numeric, errors='coerce')
    
    df["STATS.COUPLE_MILITAIRE"] = df['STATS.COUPLE_MILITAIRE'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.PLONGE'] = df['EMPSPEC.PLONGE'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.ISA'] = df['EMPSPEC.ISA'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.NEDEX'] = df['EMPSPEC.NEDEX'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.MAERO'] = df['EMPSPEC.MAERO'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.PFEU'] = df['EMPSPEC.PFEU'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.SECCIV'] = df['EMPSPEC.SECCIV'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.ISSA'] = df['EMPSPEC.ISSA'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.SMA'] = df['EMPSPEC.SMA'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.EMBQ'] = df['EMPSPEC.EMBQ'].apply(pd.to_numeric, errors='raise')
    df['EMPSPEC.ATOM'] = df['EMPSPEC.ATOM'].apply(pd.to_numeric, errors='raise')

    df['ENGOPS.CSCHMI'] = df['ENGOPS.CSCHMI'].apply(pd.to_numeric, errors='raise')
    df['ENGOPS.AOPER'] = df['ENGOPS.AOPER'].apply(pd.to_numeric, errors='raise')
    df['ENGOPS.ISAPB_NBJOURS'] = df['ENGOPS.ISAPB_NBJOURS'].apply(pd.to_numeric, errors='raise')
    df['ENGOPS.CAMP'] = df['ENGOPS.CAMP'].apply(pd.to_numeric, errors='raise')
    df['ENGOPS.OPEX'] = df['ENGOPS.OPEX'].apply(pd.to_numeric, errors='raise')

    dict_correction = {"Caporal - Chef": "Caporal-chef", 
                      "Adjudant - Chef":"Adjudant-chef",
                      "Sergent - Chef":"Sergent-chef",
                      "Lieutenant - colonel":"Lieutenant-colonel",
                      "Sous lieutenant":"Sous-lieutenant",
                      
                      "Caporal - chef":"Caporal-chef",
                      "Caporal-chef de 1ère classe":"Caporal-chef",
                      "Officers subalternes":"Officiers subalternes",
                      "officiers subalternes":"Officiers subalternes",
                    #   "Sous Officiers subalternes":  "Sous Officiers subalternes"
                      "officiers supérieurs":"Officiers supérieurs", 
                      "militaire du rang": "Militaire du Rang",
                      "Sous offciers supérieurs": "Sous officiers supérieurs",
                      "Sous officiers supérieurs": "Sous Officiers supérieurs",
                      "12":"Officiers supérieurs", 
                      "13":"Officiers subalternes", 
                      "24": "Sous officiers supérieurs", 
                      "sous offciers subalternes": "Sous Officiers subalternes", 
                      "Officiers subalterne": "Officiers subalternes"
                      }

    df['CLAIR_GRADE_SGM'] = df['CLAIR_GRADE_SGM'].replace(dict_correction)
    df['INFO_CATEGORIE'] = df['INFO_CATEGORIE'].replace(dict_correction)
 
    df['CLAIR_ARMEE'] = df['CLAIR_ARMEE'].apply(lambda x: x.replace("_x000D_", "").replace("_x005F","").strip())
 
    return df

def rename_column(df):
    new_to_old_columns_dict = {val:key for key, value in new_to_old_columns.items() for val in value}
    df.rename(columns=new_to_old_columns_dict, inplace=True)

    return df


def convert_brut_to_format(df):
    df = remove_data(df)
    for name, typ in df.dtypes.items():
        print(name, typ)

    df = preprocess(df)
    df = calcule_gain_perte(df)
    return df


def find_armee(canevas_path, semis_path):
    dict_file = {name: {"canevas": list(), "semis": list()} for name in list_armee}

    for filename in Path(canevas_path).rglob('*.xlsx'):
        str_filename_upper =  str(filename.name).upper().replace("_", " ")

        index_armee = [i for i, j in enumerate(list_armee) if j in str_filename_upper]
        print(str_filename_upper, index_armee)
        name_armee = [list_armee[ind] for ind in index_armee][0] # on prend la 1ere armée trouvée attention, à vérifier que c'est correct
        
        key = "canevas"  if "CANEVAS" in str_filename_upper else "semis"

        dict_file[name_armee][key].append(filename)

    for filename in Path(semis_path).rglob('*.xlsx'):
        str_filename_upper =  str(filename.name).upper().replace("_", " ")
        # str_direname_upper =  str(filename.parent).upper()       
        index_armee = [i for i, j in enumerate(list_armee) if j in str_filename_upper]
        print(str_filename_upper, index_armee)
        name_armee = [list_armee[ind] for ind in index_armee][0] # 
        
        key = "canevas"  if "CANEVAS" in str_filename_upper else "semis"
        dict_file[name_armee][key].append(filename)

    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(dict_file)
    return dict_file


def check_id_different(df_merge):
    diff_id_semis = set(set(df_merge["canevas"]['Libellé'])| set(df_merge["semis"]['Types'])) - set(df_merge["canevas"]['Libellé'])
    diff_id_canevas = set(set(df_merge["semis"]['Types'])| set(df_merge["canevas"]['Libellé'])) - set(df_merge["semis"]['Types'])
    
    if len(diff_id_canevas) > 0 or len(diff_id_semis) > 0:
        nb_log_diff = 100
        print("-"*10, "[DETECTION DIFFFERENCE ID lors de la fusion CANEVAS-SEMIS]", "-"*10)
        print(f"Nb différences par rapport au CANEVAS: {len(diff_id_semis)} \nNb différences par rapport au SEMIS: {len(diff_id_canevas)}")
        print("[CANEVAS] les 10 premiers:\n",'\n '.join(str(diff) for diff in list(diff_id_canevas)[:nb_log_diff]))
        with open("liste_diff_canevas.txt", 'w') as fic:
            fic.write('\n '.join(str(diff) for diff in list(diff_id_canevas)[:]))
        print("[SEMIS] les 10 premiers:\n", '\n '.join(str(diff) for diff in list(diff_id_semis)[:nb_log_diff]))
        with open("liste_diff_semis.txt", 'w') as fic:
            fic.write('\n '.join(str(diff) for diff in list(diff_id_semis)[:]))
        print("-"*10, "[FIN DETECTION DIFFFERENCE]", "-"*10)
    return None


def check_id_unique(df, nom_colonne, remove_duplicate=True):
    count_id = df[nom_colonne].value_counts()
    count_id_doublon = count_id[count_id > 1]
    if (count_id_doublon.shape[0] > 0):
        print("-"*10, " [DOUBLON] ", "-"*10)
        print(f"Sur la colonne {nom_colonne}, nb individus en doublons :{count_id_doublon.shape[0]}, total lignes {count_id_doublon.sum()}")
        for doublon,  id in count_id_doublon.items():
            print( doublon,  id)
        print("-"*10, "FIN DOUBLON", "-"*10)
    if remove_duplicate:
        df = df.drop_duplicates(subset=nom_colonne)
    return df


def duplication_terre():
    cond_terre = df_merge_formate["armee_x"] == "TERRE"
    df_merge_formate["Libellé_id"] = df_merge_formate["Libellé"].astype(str) 
    df_merge_formate["CLAIR_ARMEE_PROJ"] = df_merge_formate["CLAIR_ARMEE"]
    df_terre_brut = df_merge_formate[cond_terre]
    df_terre_brut["CLAIR_ARMEE_PROJ"]= df_terre_brut["CLAIR_ARMEE"] + "_BRUT" 
    
    brut_shape = df_terre_brut.shape
    if brut_shape[0] != 0:
        df_terre_proj = pd.concat([df_terre_brut]*3, ignore_index=True)
        df_terre_proj["CLAIR_ARMEE_PROJ"] = df_terre_proj["CLAIR_ARMEE"] + "_PROJ" 

        nb_ind_remove = (df_terre_proj.shape[0] + df_terre_brut.shape[0]) - 119693 #  cible_terre  =119693 
        np.random.seed(10)
        drop_indices = np.random.choice(df_terre_proj.index, nb_ind_remove, replace=False)
        df_shape = df_terre_proj.shape
        df_terre_proj = df_terre_proj.drop(drop_indices)

        df_terre_sur_ech = pd.concat([df_terre_proj, df_terre_brut], ignore_index=True)

        print(f"Remove {nb_ind_remove}")
        print(f"{brut_shape, df_terre_proj.shape, df_shape, df_terre_sur_ech.shape}")
        df_terre_sur_ech["Libellé_id"] = df_terre_sur_ech.index.astype('str')  + "_" + df_terre_sur_ech["Libellé"].astype(str) 
        
        df_sans_terre = df_merge_formate[df_merge_formate['armee_x'] != "TERRE"]
        df_sur_ech = pd.concat([df_terre_sur_ech, df_sans_terre] , ignore_index=True, sort=False) 
    
        nom_fichier_formate = os.path.join(output_path,name_scenario + "_CANEVAS_SEMIS_FUSION_FORMATE_SURECH.csv")
        df_sur_ech.to_csv(nom_fichier_formate, index=False)
        # df_terre_sur_ech = df_sur_ech[df_sur_ech['armee'] == "TERRE"]
        print(f"Sauvegarde de {nom_fichier_formate} de forme {df_sur_ech.shape} dont {df_terre_sur_ech.shape}")
    else:
        df_sur_ech = df_merge_formate


def merge(canevas_path, semis_path, output_path, name_scenario = "NPRM", read_csv=False):
    dict_file_by_armee = find_armee(canevas_path, semis_path)
    date_str = datetime.now().strftime("%d%m%Y") #todo à modifier 

    info_to_save = []
    # merge par canevas/par semis
    df_merge = defaultdict(lambda: None)
    for type_file in ["canevas", "semis"]:
        preced_col = None
        key_id = 'Libellé' if type_file == "canevas" else 'Types' 
        for ind, (armee, files) in enumerate(dict_file_by_armee.items()):
            # if ind > 1:
            #     continue
            for file_by_armee in files[type_file] :
                print(file_by_armee)
                if file_by_armee is not None:
                    ind_header = 0 if type_file == "canevas" else 2 # la premiere ligne est du texte pour les sorties

                    if read_csv==True:
                        file_by_armee_csv = str(file_by_armee).replace(".xlsx", ".csv")
                        df = pd.read_csv(file_by_armee_csv,  header=ind_header, encoding='utf-8')
                    else:
                        df = pd.read_excel(file_by_armee,  header=ind_header, encoding='utf-8')#, index=False)
                        df.to_csv(str(file_by_armee).replace(".xlsx", ".csv"), index=False)
                    
                    print(df.head())
                    if type_file == "canevas":
                        df = df.drop(df.index[0]) #retire la première ligne qui contient la description des entêtes pour le canevas
                    
                    df = df[~df[key_id].isna()]

                    df[key_id] = df[key_id].apply(lambda x: x.replace("(ERREUR)","").replace(";",""))
                    df[key_id] = df[key_id].apply(lambda x: x[:100])
                    df[key_id] = df[key_id].str.strip()
                    df = rename_column(df)
                    
                                        
                    df = check_id_unique(df, key_id)
                    df['armee'] = armee
                    print(armee, df.shape)
                    
                    diff_colonne = set(preced_col) - set(df.keys()) if preced_col is not None else None
                    print(armee, df.shape, diff_colonne)
                    info_to_save.append((os.path.basename(file_by_armee), type_file, armee, df.shape))

                    df_merge[type_file] = df if df_merge[type_file] is None else  pd.concat([df_merge[type_file], df], sort=False)
                    
                    preced_col = df_merge[type_file].keys()
                else:
                    print(armee, " ne contient aucun fichier pour ",type_file )
        nom_deb_fichier =  'CANEVAS' if type_file == "canevas" else 'SEMIS' 
        nom_fichier = os.path.join(output_path, name_scenario + "_" + nom_deb_fichier + "_FUSION.csv")
        
        if df_merge[type_file] is not None:
            df_merge[type_file][key_id] = df_merge[type_file][key_id].apply(lambda x: x[:100].replace(";",""))
            df_merge[type_file][key_id] = df_merge[type_file][key_id].str.strip()

            df_merge[type_file].to_csv(nom_fichier, index=False)

            print(f"Sauvegarde de {nom_fichier} de forme {df_merge[type_file].shape} \n\n")
            info_to_save.append((os.path.basename(nom_fichier), type_file, "ALL", df_merge[type_file].shape))

    check_id_different(df_merge) # print les différences, indiquer dans la colonne "outer"
    
    df_merge_all = pd.merge(df_merge["canevas"], df_merge["semis"],  left_on='Libellé', right_on='Types', how='outer', indicator=True) 

    nom_fichier_all = os.path.join(output_path, name_scenario + "_CANEVAS_SEMIS_FUSION.csv")
    df_merge_all.to_csv(nom_fichier_all, index=False)
    print(f"Sauvegarde de {nom_fichier_all} de forme {df_merge_all.shape}")
    info_to_save.append(("MERGE", "ALL", df_merge_all.shape))

    df_merge_formate = convert_brut_to_format(df_merge_all)
    nom_fichier_formate = os.path.join(output_path, name_scenario + "_CANEVAS_SEMIS_FUSION_FORMATE.csv")
    df_merge_formate.to_csv(nom_fichier_formate, index=False)
    print(f"Sauvegarde de {nom_fichier_formate} de forme {df_merge_formate.shape}")
    info_to_save.append(("MERGE", "ALL_FORMATE", df_merge_formate.shape))
    
    df_debug = pd.DataFrame(info_to_save)
    df_debug.to_csv("info_debug.csv")
    
             
def main():
    parser = argparse.ArgumentParser(usage="merge -c path_canevas -s path_semis")
    parser.add_argument("--canevas", "-c", help="Dossier où sont les fichiers à merger")
    parser.add_argument("--semis", "-s", help="Dossier où sont les fichiers à merger")
    parser.add_argument("--output", "-o", help="Chemin où sont enregistrer les fichiers de sortie", default="./")
    parser.add_argument("--scenario", help="Debut du nom des fichiers", default="SCENARIO_6")
    parser.add_argument("--read_csv", help="Lecture des csv au lieu des xlsx quand le merge a déjà été fait", action='store_true')
    args = parser.parse_args()

    merge(args.canevas, args.semis, args.output, args.scenario, args.read_csv)

if __name__ == "__main__":
    main()
